CREATE DATABASE  IF NOT EXISTS `BlazeDB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `BlazeDB`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (i686)
--
-- Host: 127.0.0.1    Database: BlazeDB
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `b1_app_device_details`
--

DROP TABLE IF EXISTS `b1_app_device_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_app_device_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_device_token_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `act_status` tinyint(1) DEFAULT '1',
  `sessionId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_app_device_details`
--

LOCK TABLES `b1_app_device_details` WRITE;
/*!40000 ALTER TABLE `b1_app_device_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_app_device_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_connected_devices`
--

DROP TABLE IF EXISTS `b1_connected_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_connected_devices` (
  `connect_device_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `hub_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`connect_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_connected_devices`
--

LOCK TABLES `b1_connected_devices` WRITE;
/*!40000 ALTER TABLE `b1_connected_devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_connected_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_device_categories`
--

DROP TABLE IF EXISTS `b1_device_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_device_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_parent_id` int(11) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_desc` varchar(255) DEFAULT NULL,
  `category_type` enum('Security','Remote','Music','Devices') DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_device_categories`
--

LOCK TABLES `b1_device_categories` WRITE;
/*!40000 ALTER TABLE `b1_device_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_device_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_devices`
--

DROP TABLE IF EXISTS `b1_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_make` varchar(255) DEFAULT NULL,
  `device_radio_type` varchar(255) DEFAULT NULL,
  `device_state` enum('ADDED','DELETED') DEFAULT 'ADDED',
  `device_create_date` datetime DEFAULT NULL,
  `device_delete_date` datetime DEFAULT NULL,
  `device_deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_devices`
--

LOCK TABLES `b1_devices` WRITE;
/*!40000 ALTER TABLE `b1_devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_hub_definition`
--

DROP TABLE IF EXISTS `b1_hub_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_hub_definition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hub_id` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_desc` varchar(255) DEFAULT NULL,
  `emergency_name` varchar(255) DEFAULT NULL,
  `emergency_phone` varchar(255) DEFAULT NULL,
  `emergency_email` varchar(255) DEFAULT NULL,
  `hub_longitude` float DEFAULT NULL,
  `hub_latitude` float DEFAULT NULL,
  `no_of_rooms` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_hub_definition`
--

LOCK TABLES `b1_hub_definition` WRITE;
/*!40000 ALTER TABLE `b1_hub_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_hub_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_hub_room`
--

DROP TABLE IF EXISTS `b1_hub_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_hub_room` (
  `hub_room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `hub_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hub_room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_hub_room`
--

LOCK TABLES `b1_hub_room` WRITE;
/*!40000 ALTER TABLE `b1_hub_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_hub_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_hub_users`
--

DROP TABLE IF EXISTS `b1_hub_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_hub_users` (
  `user_hub_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `hub_id` varchar(255) DEFAULT NULL,
  `user_type` enum('master','guest') DEFAULT 'master',
  `device` enum('1','0') DEFAULT '1',
  `music` enum('1','0') DEFAULT '1',
  `remote` enum('1','0') DEFAULT '1',
  `security` enum('1','0') DEFAULT '1',
  `security_device_category` enum('ARM','INHOUSE','PANIC','EMERGENCY') DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_hub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_hub_users`
--

LOCK TABLES `b1_hub_users` WRITE;
/*!40000 ALTER TABLE `b1_hub_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_hub_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_hubs`
--

DROP TABLE IF EXISTS `b1_hubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_hubs` (
  `hub_id` varchar(255) NOT NULL DEFAULT '',
  `model_no` varchar(255) DEFAULT NULL,
  `date_of_mfg` varchar(255) DEFAULT NULL,
  `software_version` varchar(255) DEFAULT NULL,
  `software_install_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_hubs`
--

LOCK TABLES `b1_hubs` WRITE;
/*!40000 ALTER TABLE `b1_hubs` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_hubs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_push_notifications`
--

DROP TABLE IF EXISTS `b1_push_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_push_notifications` (
  `push_notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_device_token_id` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `act_create_date` datetime DEFAULT NULL,
  `act_type` varchar(255) DEFAULT NULL,
  `act_sent_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`push_notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_push_notifications`
--

LOCK TABLES `b1_push_notifications` WRITE;
/*!40000 ALTER TABLE `b1_push_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_push_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_room`
--

DROP TABLE IF EXISTS `b1_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(255) DEFAULT NULL,
  `room_description` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_room`
--

LOCK TABLES `b1_room` WRITE;
/*!40000 ALTER TABLE `b1_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `b1_user_details`
--

DROP TABLE IF EXISTS `b1_user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b1_user_details` (
  `user_id` varchar(255) NOT NULL DEFAULT '',
  `user_name` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `phone_no` bigint(11) DEFAULT NULL,
  `alt_phone_no` bigint(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `old_password1` varchar(255) DEFAULT NULL,
  `old_password2` varchar(255) DEFAULT NULL,
  `security_question1` varchar(255) DEFAULT NULL,
  `security_answer1` varchar(255) DEFAULT NULL,
  `security_question2` varchar(255) DEFAULT NULL,
  `security_answer2` varchar(255) DEFAULT NULL,
  `act_create_date` datetime DEFAULT NULL,
  `act_delete_date` datetime DEFAULT NULL,
  `act_authenticated` tinyint(1) DEFAULT '0',
  `act_enabled` tinyint(1) DEFAULT '1',
  `authentication_code` varchar(255) DEFAULT NULL,
  `isMaster` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_id` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b1_user_details`
--

LOCK TABLES `b1_user_details` WRITE;
/*!40000 ALTER TABLE `b1_user_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `b1_user_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-05 16:38:04
