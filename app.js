var express    = require('express'),
app        = express(),
db = require('./mySqlDB'),
mongodb = require('./db'),
bodyParser = require('body-parser'),
port = process.env.PORT || 8080,
Globals = require('./globals'),
log4js = require('log4js'),
redis = require('./redisDB'),
_ = require('underscore'),
queueObj = require('./Controllers/Commons/QueueHandler');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');


app.set('views', __dirname + '/Views');
app.engine('html', require('ejs').__express);
app.set('view engine', 'ejs');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(logResponseBody);
app.use(require('./Controllers'));

function logResponseBody(req, res, next) {
  var oldWrite = res.write,
  oldEnd = res.end,
  chunks = [],
  t1 = new Date();

  res.write = function (chunk) {
    chunks.push(chunk);
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk)
    chunks.push(chunk);

    //        var body = Buffer.concat(chunks).toString('utf8');
    var t2 = new Date();
    //        logger.trace((t2 - t1) + " : Path: " + req.path + " :Req.body:::: " + JSON.stringify(req.body) + " : ResponseBody:::: "+ body);

    oldEnd.apply(res, arguments);
  };

  next();
};


process.on('SIGINT', function() {
  mongodb.close(function(){
    logger.info('closing db');
    process.exit(0);
  });
});

process.on('uncaughtException', function(err) {
  // handle the error safely
  logger.info(err.stack);
});

mongodb.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){
  if(err){
    logger.info("Problem in connecting MongoDB.");
  }else{
    logger.info("Connected to MongoDB.");
  }
});

redis.connect(Globals.RedisHost, Globals.RedisPort, function(){
  logger.info("redis connected.");
});


db.connect(Globals.MySqlHost, Globals.MySqlPort, Globals.MySqlDB, Globals.MySqlUser, Globals.MySqlPass, function (err) {
  if (err) {
    logger.error('Unable to connect to MySql.');
    process.exit(1);
  } else {
    logger.info('connected to MySql.');
    app.listen(port, function () {
      logger.info('API\'s work at http://localhost:' + port + " url.");
      setTimeout(scheduler, Globals.graceTime);  // uncomment to track the hub on and off status
    });
  }
});

function scheduler(){
  var Models = db.getModels();
  Models.Hub.findAll({order: 'hub_id DESC'})
  .then(function(hubs){
    if(hubs[0]){
      var hubsArr, obj = [], _date = new Date();
      _.each(hubs, function(v){
        obj.push({
          hub_id: v.hub_id,
          status: ((new Date(v.hub_status_date)).getTime() < (_date.getTime() - Globals.graceTime)) ? 0 : 1,
          timestamp: _date
        });
      });
      hubsArr = _.pluck(obj, "hub_id");
      mongodb.findSortedObjects('hub_on_off_status', {hub_id: {$in: hubsArr}}, {timestamp: -1}, function(err, data){
        if(!err){
          if(data[0]){
            var _newObj = {};
            _.each(data, function(v){
              if(!_newObj[v.hub_id]){
                _newObj[v.hub_id] = v;
                var _idx = hubsArr.indexOf(v.hub_id);
                if(v.status == obj[_idx].status){
                  obj.splice(_idx, 1);
                  hubsArr.splice(_idx, 1);
                }
              }
            });
          }
          if(obj[0]){
            mongodb.save('hub_on_off_status', obj, function(){
              setTimeout(scheduler, Globals.graceTime);
            });
          }else{
            setTimeout(scheduler, Globals.graceTime);
          }
        }else{
          logger.error("error in scheduler: " + err.message);
          setTimeout(scheduler, Globals.graceTime);
        }
      });
    }
  });
}
