var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
  	id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    app_device_token_id: { type: Sequelize.STRING },
    user_id: { type: Sequelize.CHAR(36) },
    act_status: { type: Sequelize.BOOLEAN, defaultValue: true },
    sessionId: { type: Sequelize.STRING, defaultValue: null },
    device_type: { type: Sequelize.STRING, defaultValue: "IOS" }
  };
};

exports.getTableName = function(){
  return "b1_app_device_details";
};