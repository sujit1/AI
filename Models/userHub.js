var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    user_hub_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    user_id: { type: Sequelize.CHAR(36) },
    hub_id: { type: Sequelize.STRING },
    user_type: { type: Sequelize.ENUM('master', 'guest'), defaultValue: 'master' },
    device: { type: Sequelize.ENUM('1', '0'), defaultValue: '1' },
    music: { type: Sequelize.ENUM('1', '0'), defaultValue: '1' },
    remote: { type: Sequelize.ENUM('1', '0'), defaultValue: '1'},
    security: { type: Sequelize.ENUM('1', '0'), defaultValue: '1' },
    parent_id: { type: Sequelize.CHAR(36), defaultValue: null  },
    act_enabled: { type: Sequelize.BOOLEAN, defaultValue: true }
  };
};

exports.getTableName = function(){
  return "b1_hub_users";
};