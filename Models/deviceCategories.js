var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    // category_id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    category_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    category_parent_id: { type: Sequelize.CHAR(36) },
    category_name: { type: Sequelize.STRING },
    category_desc: { type: Sequelize.STRING },
    category_type: {type: Sequelize.ENUM('Security', 'Remote', 'Music', 'Devices') },
    device_make: { type: Sequelize.STRING },
    device_model: { type: Sequelize.STRING },
    device_radio_type: { type: Sequelize.STRING },
    // category_view_type: {type: Sequelize.ENUM('findBridge', 'SelectDevice', 'Webpage', 'none') }
    // category_technology: {type: Sequelize.ENUM('WIFI', 'ZIGBEE', 'Z-WAVE', 'BLE', 'none') }
  };
};

exports.getTableName = function(){
  return "b1_device_categories";
};