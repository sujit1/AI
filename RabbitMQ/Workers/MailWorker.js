var amqp = require('amqp'),
        Globals = require('../globals'),
        Mailer = require('./mail'),
        _ = require('underscore'),
        log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function MailWorker() {
    _.bindAll(this, "init", "mailWorker");
    this.connection = this.createConnection();
    this.connection.on("ready", this.init);
};

MailWorker.prototype.createConnection = function () {
    return amqp.createConnection({host: Globals.RabbitMQConfig.host, port: Globals.RabbitMQConfig.port, login: Globals.RabbitMQConfig.auth.user, password: Globals.RabbitMQConfig.auth.pass});
};

MailWorker.prototype.init = function () {
    var mailExchangeObj = false,
        mailQueueObj = false,
        exchangeOptions = {durable: true, autoDelete: false, type: 'fanout'},
        mailQueueOptions = {durable: true, autoDelete: false, closeChannelOnUnsubscribe: true},
        mailSubscribeOptions = {ack: true};
    mailExchangeObj = this.connection.exchange(Globals.RabbitMQConfig.ExchangeName, exchangeOptions);
    mailQueueObj = this.connection.queue( Globals.RabbitMQConfig.QueueName,  mailQueueOptions);
    mailQueueObj.bind(mailExchangeObj, '#');
    mailQueueObj.subscribe(mailSubscribeOptions, this.mailWorker);
};

MailWorker.prototype.mailWorker = function (payload, headers, info, msg) {
    logger.info("Processing mailWorker payload... Inside mail Queue.." + JSON.stringify(payload)); //Keep this log for debugging...
    var success = Mailer.sendMail(payload);
    if (success) {
        msg.acknowledge();
    } else {
        msg.reject(false); // reject=true, requeue=false causes dead-lettering
    }
 };


module.exports = new MailWorker();
