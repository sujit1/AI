var amqp = require('amqp'),
        Globals = require('../globals'),
        Notification = require('./notification'),
        GCM = require('./gcm'),
        _ = require('underscore');

function PushNotificationWorker() {
    _.bindAll(this, "init", "notification");
    this.connection = this.createConnection();
    this.connection.on("ready", this.init);
};

PushNotificationWorker.prototype.createConnection = function () {
    return amqp.createConnection({host: Globals.RabbitMQConfig.host, port: Globals.RabbitMQConfig.port, login: Globals.RabbitMQConfig.auth.user, password: Globals.RabbitMQConfig.auth.pass});
};

PushNotificationWorker.prototype.init = function () {
    var notificationExchangeObj = false,
            notificationQueueObj = false,
            exchangeOptions = {durable: true, autoDelete: false, type: 'fanout'},
    notificationQueueOptions = {durable: true, autoDelete: false, closeChannelOnUnsubscribe: true},
    notificationSubscribeOptions = {ack: true};
    notificationExchangeObj = this.connection.exchange(Globals.RabbitMQConfig.PushNotificationExchangeName, exchangeOptions);
    notificationQueueObj = this.connection.queue(Globals.RabbitMQConfig.PushNotificationQueueName, notificationQueueOptions);
    notificationQueueObj.bind(notificationExchangeObj, '#');
    notificationQueueObj.subscribe(notificationSubscribeOptions, this.notification);
};

PushNotificationWorker.prototype.notification = function (payload, headers, info, msg) {
    console.log((new Date()) + " Processing notification payload... Inside notification Queue..msg: " + msg + " : payload: " + JSON.stringify(payload)); //Keep this log for debugging...
    var success_IOS, success_ANDROID; //, deviceToken_IOS = "", deviceToken_ANDROID="";

//    if(payload.hasOwnProperty("deviceToken_IOS") && payload.deviceToken_IOS.length){
//        var deviceToken_IOS_LIST = payload.deviceToken_IOS.split(",");
//        for(var initial = 0; initial< deviceToken_IOS_LIST.length; initial++){
//            var dToken = deviceToken_IOS_LIST[initial];
//            if(dToken.length === 152){ 
//            //if(dToken.length === 152 && dToken.split(":").length === 2){
//                deviceToken_ANDROID+= dToken;
//            }else if(dToken.length === 64){
//                deviceToken_IOS+= dToken;
//            }else{
//                console.log("Unknown token in IOS:"+dToken);
//            }
//        }
//    }
//    
//    if(payload.hasOwnProperty("deviceToken_ANDROID") && payload.deviceToken_IOS.length){
//        var deviceToken_ANDROID_LIST = payload.deviceToken_ANDROID.split(",");
//        for(var initial = 0; initial< deviceToken_ANDROID_LIST.length; initial++){
//            var dToken = deviceToken_ANDROID_LIST[initial];
//            if(dToken.length === 152){ 
//            //if(dToken.length === 152 && dToken.split(":").length === 2){
//                deviceToken_ANDROID+= dToken;
//            }else if(dToken.length === 64){
//                deviceToken_IOS+= dToken;
//            }else{
//                console.log("Unknown token in ANDROID:"+dToken);
//            }
//        }
//    }
//    payload.deviceToken_IOS = deviceToken_IOS;
//    payload.deviceToken_ANDROID = deviceToken_ANDROID;

    if (payload.deviceToken_IOS.length > 0)
        success_IOS = Notification.alert(payload);

    if (payload.deviceToken_ANDROID.length > 0)
        success_ANDROID = GCM.alert(payload);

    if (success_IOS || success_ANDROID) {
        msg.acknowledge();
    } else {
        msg.reject(false); // reject=true, requeue=false causes dead-lettering
    }
};

module.exports = new PushNotificationWorker();
