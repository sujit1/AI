/* 
  queue.js
  @Govardhan Rao Ganji - govardhanforvm@gmail.com 
*/
//Global variables
// ==============================================
var amqp = require('amqplib/callback_api');
var Globals = require('../../globals');

var Queue = function(){
  this.url = 'amqp://'+Globals.queue.auth.user+':'+Globals.queue.auth.pass+'@'+ Globals.queue.host+':'+ Globals.queue.port;
};

// Push/ insert objects to queue
// ==============================================
Queue.prototype.push= function(queueName, message, callback) {
    amqp.connect(this.url, function(err, conn) {
    if(err && callback) callback(err);
    else{
      conn.createChannel(function(err, ch) {
        if(err) callback(err);                                                                                                                                                                                                                                  
        else{
          ch.assertQueue(queueName, {durable: false});
          ch.sendToQueue(queueName, new Buffer(message));
          console.log(" [x] Sent '"+ message +"'");
          if(callback)
              callback(null,"Message sent successfully.");
        }
      });
    }
  });
};

//Pull/ get objects from queue
// ==============================================
Queue.prototype.pull = function(queueName, callback) {
  amqp.connect(this.url, function(err, conn) {
    if(err && callback) callback(err); 
    else{
      conn.createChannel(function(err, ch) {
        if(err) callback(err);
        else{
          ch.assertQueue(queueName, {durable: false});
          console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queueName);
            ch.consume(queueName, function(msg) {
              console.log("Data :"+msg.content.toString().split('.'));
              console.log("Data Length:"+msg.content.toString().split('.').length -1 );
              console.log(" [x] Received %s", msg.content.toString());
              if(callback)
                  callback(null, msg.content.toString());
            }, {noAck: true});
          }
        });
      }
  });
};

// ==============================================
module.exports = new Queue();
