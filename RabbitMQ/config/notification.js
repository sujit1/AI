/* 
  mail.js
  @Govardhan Rao Ganji - govardhanforvm@gmail.com 
*/
//Global variables
// ==============================================
var apn = require("apn");
var Globals = require('../../globals');

var Notification = function(){
  this.options = {
          cert: Globals.notification.apple.cert,
          certData: null,
          key: Globals.notification.apple.key,
          keyData: null,
          ca: null,
          pfx: null,
          pfxData: null,
          passphrase: Globals.notification.apple.passphrase,
          address: Globals.notification.apple.url.notification,
          port: Globals.notification.apple.port.notification,
          rejectUnauthorized: true,
          enhanced: true,
          cacheLength: 100,
          autoAdjustCache: true,
          maxConnections: 1,
          connectionTimeout: 0,
          buffersNotifications: true,
          fastMode: false,
          legacy: false
  };
  this.apnConnection = new apn.Connection(this.options);
};

Notification.prototype.alert= function(jsonObject){
    if(jsonObject && jsonObject.hasOwnProperty("deviceToken")) {
        return "Please provide device token";
    }else{
        var deviceTokens = jsonObject.deviceToken.split(',');
        var devices = [];
        
        if(deviceTokens.length === 1){
            var device = new apn.Device(jsonObject.deviceToken.trim());
            devices.push(device);
          }else{
            for(var initial =0; initial < deviceTokens.length; initial++){
              var device = new apn.Device(deviceTokens[initial].trim());
              devices.push(device);
            }
          }
        
//        var myDevice = new apn.Device(jsonObject.deviceToken);
        var note = new apn.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.badge = 1;
        note.sound = "ping.aiff";
        note.alert = jsonObject.message;
        note.payload = {'messageFrom': 'Blaze B One'};
//        this.apnConnection.pushNotification(note, myDevice);
        this.apnConnection.pushNotification(note, devices);

        return true;
    }
};

Notification.prototype.feedback= function(){
    var options = this.options;
    options.address= Globals.notification.apple.url.feedback;
    options.port= Globals.notification.apple.port.feedback;
    options.batchFeedback= true;
    options.interval= 1000;
    options.production= false;
    options.feedback= true;

  var feedback = new apn.Feedback(options);
    feedback.on("feedback", function(devices) {
        devices.forEach(function(item) {
            var device = new apn.Device(item.device.token);
            // Do something with item.device and item.time;
            console.log(item.time+" >>  >> "+device);
        });
    });
};

// ==============================================
module.exports = new Notification();

