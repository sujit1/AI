var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    hub_contact_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    hub_id: { type: Sequelize.STRING },
    emergency_name: { type: Sequelize.STRING },
    emergency_phone: { type: Sequelize.STRING },
    emergency_email: { type: Sequelize.STRING }
  };
};

exports.getTableName = function(){
  return "b1_hub_emergency_contacts";
};