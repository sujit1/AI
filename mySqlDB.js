var Sequelize = require('sequelize');
var Users = require('./Models/users');
var UserHub = require('./Models/userHub');
var UserDev = require('./Models/userDevice');
var DeviceCategory = require('./Models/deviceCategories');
var Devices = require('./Models/devices');
var Hub = require('./Models/hub');
var HubDef = require('./Models/hubDefinition');
var HubRoom = require('./Models/hubRoom');
var Room = require('./Models/room');
var HubEmergencyContacts = require('./Models/hubEmergencyContacts');
var RemoteKeys = require('./Models/remoteKeys');
var AlexaUser = require('./Models/alexaUser');
var LineUser=require('./Models/lineUser');

var models = {};

exports.connect = function(host, port, dbName, userName, password, cb){
    var sequelize = new Sequelize(dbName, userName, password, {
        // default db values
        // host: localhost,
        // port: 3306,
        host: host,
        port: port,
        dialect: 'mysql',
        logging: false,
        syncOnAssociation: true,
        define: { timestamps: false }
    });
    models.User = sequelize.define('User', Users.getSchema(), {tableName: Users.getTableName()});
    models.UserHub = sequelize.define('UserHub', UserHub.getSchema(), {tableName: UserHub.getTableName()});
    models.UserDev = sequelize.define('UserDev', UserDev.getSchema(), {tableName: UserDev.getTableName()});
    models.DeviceCategory = sequelize.define('DeviceCategory', DeviceCategory.getSchema(), {tableName: DeviceCategory.getTableName()});
    models.Devices = sequelize.define('Devices', Devices.getSchema(), {tableName: Devices.getTableName()});
    models.Hub = sequelize.define('Hub', Hub.getSchema(), {tableName: Hub.getTableName()});
    models.HubDef = sequelize.define('HubDef', HubDef.getSchema(), {tableName: HubDef.getTableName()});
    models.HubRoom = sequelize.define('HubRoom', HubRoom.getSchema(), {tableName: HubRoom.getTableName()});
    models.Room = sequelize.define('Room', Room.getSchema(), {tableName: Room.getTableName()});
    models.HubEmergConts = sequelize.define('HubEmergConts', HubEmergencyContacts.getSchema(), {tableName: HubEmergencyContacts.getTableName()});
    models.RemoteKeyInfo = sequelize.define('RemoteKeyInfo', RemoteKeys.getSchema(), {tableName: RemoteKeys.getTableName()});
    models.AlexaUser = sequelize.define('AlexaUser', AlexaUser.getSchema(), {tableName: AlexaUser.getTableName()});
    models.LineUser = sequelize.define('LineUser', LineUser.getSchema(), {tableName: LineUser.getTableName()});

    // sequelize.sync({force:true}).then(function(){    // Note: This will delete the tables and create them again.
    sequelize.sync().then(function(){
        cb();
    }).catch(function(err){
        console.log("Error while connecting to Db: " + err.message);
    });
};

exports.getModels = function(){
    return models;
};

exports.getUserModel = function(){
    return models.User;
};

exports.getUserHubModel = function(){
    return models.UserHub;
};

exports.getUserDeviceModel = function(){
    return models.UserDev;
};
