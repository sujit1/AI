var Path = require('path');

function Globals() {
};
Globals.prototype.MailerAuthenticationSubject = "Blaze B.One - Registration Successful";
Globals.prototype.MailerInviteGuestSubject = "Blaze B.One - Invitation to collaborate";
Globals.prototype.MailerForgotPasswordSubject = "Blaze B.One - Forgot password reset";
Globals.prototype.MailerReportSubject = "Blaze B.One - Registered Report";
//DB configurations
Globals.prototype.MongoPort = 27017;
//Current production server
Globals.prototype.MongoHost = '172.31.41.198'; // '54.86.166.212';
Globals.prototype.MongoDB = 'blazedb';
//        Old production server
//  Globals.prototype.MongoHost = '172.31.12.148'; //' 52.90.18.230';
// Globals.prototype.MongoDB = 'test1';

//RabbitMQ configuration
Globals.prototype.RabbitMQConfig = {
    host: '127.0.0.1',
    port: 5672,
    auth: {
//        Current production server
     user: 'guest',
      pass: 'guest'
//        Old production server
       //  user: 'admin',
        // pass: 'bz@123'
    },
    ExchangeName: "TestMessageExchange",
    QueueName: "TestMessageQueue",
    PushNotificationExchangeName: "PushNotificationTestMessageExchange",
    PushNotificationQueueName: "PushNotificationQueue",
    EventExchangeName: "TestEventMessageExchange",
    EventQueueName: "TestEventQueue"
};

// Template configuration
// ==============================================
Globals.prototype.template = {
    forgotPassword: 'forgotPassword.jade',
    registration: 'registration.jade',
    invitation: 'invitation.jade',
    resolved: 'resolved.jade',
    created: 'created.jade'
};

//Mysql DB configurations
Globals.prototype.MySqlPort = 3306;
Globals.prototype.MySqlDB = "BlazeDB";
Globals.prototype.MySqlUser = "root";
//Current production server
Globals.prototype.MySqlHost = '172.31.41.198'; // '54.86.166.212';
Globals.prototype.MySqlPass = "blaze";
//        Old production server
//  Globals.prototype.MySqlHost = '172.31.12.148'; //' 52.90.18.230';
 //Globals.prototype.MySqlPass = "bl@z3";

//Redis DB configurations
Globals.prototype.RedisPort = 6379;
//Current production server
Globals.prototype.RedisHost = '172.31.41.198'; // '54.86.166.212';
//        Old production server
//  Globals.prototype.RedisHost = '172.31.12.148'; //' 52.90.18.230';

// Mail images
Globals.prototype.MailLogo = "/images/mail_japan.jpg";
Globals.prototype.appRoot = Path.resolve(__dirname);
Globals.prototype.maxContacts = 8;
Globals.prototype.maxBOneIdChars = 4;

Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 10,
            "category": "relative-logger"
        }
    ]
};

Globals.prototype.adminMail = "vivek@blazeautomation.com";
//Globals.prototype.adminMail = "sujit@blazeautomation.com";


Globals.prototype.maxMongoRecordCount = 30000;
Globals.prototype.resetMongoRecordCount = 10000;

Globals.prototype.maxDummyHubsDigitLength = 10;

Globals.prototype.graceTime = 120000; // in milliseconds used for hub on off status schedule time
Globals.prototype.beseyeDevClientId="9f7e7b9d5b2ebafbd99b944870276b5d0567e8061467301b9cf9458c228aa4b4";
Globals.prototype.beseyeDevClientSecret="49728bd323fb9dc3bf1caa284ac674735e88ff35b85428276f077872fc64c1a7";

module.exports = new Globals();
