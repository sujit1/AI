var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , Globals = require('../../globals')
    , Sequelize = require('sequelize')
    , User = require('../User/Users')
    , Utils = require('../Commons/Utils')
    , log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Auth() {
    _.bindAll(this, "getModels", "postLogin", "getLogin", "accessToken", "validateAlexa");

    router.post('/login', this.postLogin);
    router.get('/login', this.getLogin);
    router.post('/accessToken', this.accessToken);

    return router;
};

Auth.prototype.postLogin = function (req, res) {
    logger.trace('postLogin start');
    var body = req.body;
    var obj = {email_id: body.email.toLowerCase()}, that = this;
    that.getModels();
    if(body.state !== '' && body.client_id !== '' && body.scope !== '' && body.response_type !== ''){
      that.userModel.findAll({where: obj})
          .then(function (arr) {
              if (arr.length === 0) {
                logger.trace('postLogin End arr 0.');
                //TODO: given credentials not matched.
                body.title = "B One";
                body.message = "Invalid email & password, please try once again.";
                res.render('index.html', body);
              } else {
                  logger.trace('postLogin End arr 0.'+ arr.length);
                  user = arr[0];
                  if(user){
                    if (user.password === User.encryptPassword(body.password)){
                        logger.trace('Login success');
                        that.validateAlexa(user, body, res);
                    }else{
                      //TODO: Password not match
                      body.title = "B One";
                      body.message = "Invalid password, please try once again.";
                      res.render('index.html', body);
                    }
                  }
              }
          })
          .catch(function(err){
              logger.error(err.message);
              body.title = "B One";
              body.message = "Somthing went wrong, please try once again.";
              res.render('index.html', body);
          });
    }else{
      body.title = "B One";
      body.message = "Request with invalid data.";
      res.render('index.html', body);
    }
    logger.trace('postLogin End');
};

Auth.prototype.getLogin = function (req, res) {
    var responseData = req.query;
    responseData.title = "B One";
    res.render('index.html', responseData);
};

Auth.prototype.accessToken = function(req, res, next) {
    logger.trace('accessToken start');
    logger.trace('Body Data :'+JSON.stringify(req.body));
    var body = req.body, that = this;
    that.getModels();
    var _alexaUser = {session_id: body.code};
    that.alexaUserModel.findAll({where: _alexaUser})
        .then(function (alexaUserArray) {
            if (alexaUserArray.length === 0) {
                logger.error("Alexa user length 0.");
            } else {
                var alexaUser = alexaUserArray[0];
                logger.trace("Alexa user api came and asking for new refresh_token.");
                // new user.
                // var access_token = User.generateSessionId();
                var refresh_token = User.generateSessionId();
                alexaUser.update({ refresh_token: refresh_token})
                  .then(function(){
                      if(res){
                        res.send({ access_token: alexaUser.access_token, expires_in: 0, refresh_token: refresh_token });
                      }
                  });
            }
        })
        .catch(function(err){
            logger.error(err.message);
        });
    logger.trace('accessToken End');
};

Auth.prototype.validateAlexa = function(user, reqBody, res) {
    var alexa = {
            email_id: user.email_id
        },
        that = this;
    that.getModels(), hub = null, code = null, body = reqBody;
    that.alexaUserModel.findAll({
            where: alexa
        })
        .then(function(alexaUserArray) {
            if (alexaUserArray.length === 0) {
                logger.trace('Login success new alexaUserArray 0.');

                //TODO: New user save
                that.userHubModel.findAll({
                        where: {
                            user_id: user.user_id,
                            act_enabled: true
                        },
                        hub_id: {
                            $and: {
                                $ne: null,
                                $ne: ""
                            }
                        }
                    })
                    .then(function(hubs) {
                        if (hubs.length === 0) {
                            body.title = "B One";
                            body.message = "No hubs added to this account. Please add the hub and try again.";
                            res.render('index.html', body);
                        } else {
                            hub = hubs[0];
                            var sessionId = User.generateSessionId();
                            // code = sessionId;
                            code = user.user_id + "_"; //user_id in 0
                            code += sessionId + "_"; //sessionid in 1
                            logger.trace("2 HUB :" + JSON.stringify(hub));
                            if (hub !== undefined && hub !== null) {
                                code += hub.hub_id; // hub_id in 2

                                var _alexaUser = {
                                    user_id: user.user_id,
                                    state: body.state,
                                    client_id: body.client_id,
                                    scope: body.scope,
                                    response_type: body.response_type,
                                    session_id: code,
                                    email_id: user.email_id,
                                    access_token: code
                                };
                                var alexaUser = that.alexaUserModel.build(_alexaUser);
                                alexaUser.save()
                                    .then(function() {
                                        if (res) {
    //  res.redirect('https://pitangui.amazon.com/api/skill/link/M16NCQ995CQJFR?state=' + _alexaUser.state + '&code=' + _alexaUser.session_id);
    
    res.redirect('https://pitangui.amazon.com/api/skill/link/M2005KTMHCZ024?state=' + _alexaUser.state + '&code=' + _alexaUser.session_id);  
                                      }
                                    })
                                    .catch(function(err) {
                                        logger.error(err.message);
                                        body.title = "B One";
                                        body.message = "Somthing went wrong, please try once again.";
                                        res.render('index.html', body);
                                    });
                            }
                        }
                    })
                    .catch(function(err) {
                        body.title = "B One";
                        body.message = "Somthing went wrong, please try once again.";
                        res.render('index.html', body);
                    });
            } else {
                logger.trace('Login success alexaUserArray morethan 0.' + alexaUserArray.length);
                //TODO: Update the existing user data.
                var alexaUser = alexaUserArray[0];
                logger.trace('Login success alexaUserArray 0.' + JSON.stringify(alexaUser));
                var count = alexaUser.count + 1; //Count will says, how many times user enables the B One Smart Home Skill.

                logger.trace("BODY STINGIFY :" + JSON.stringify(body));
                logger.trace("FLAG STATUS :" + (alexaUser.session_id !== undefined && alexaUser.session_id !== null));
                logger.trace("FLAG STATUS :" + (alexaUser.session_id !== undefined && alexaUser.session_id !== null && alexaUser.session_id.split("_").length === 3));
                if (alexaUser.session_id !== undefined && alexaUser.session_id !== null && alexaUser.session_id.split("_").length === 3) {
                    logger.trace('Login success alexaUserArray 0.' + JSON.stringify(alexaUser)+ " valid session_id"+alexaUser.session_id);

                    var _alexaUser = {
                      count: count
                    };

                  alexaUser.update(_alexaUser)
                      .then(function() {
                          if (res) {
          //  res.redirect('https://pitangui.amazon.com/api/skill/link/M16NCQ995CQJFR?state=' + body.state + '&code=' + alexaUser.session_id);
        res.redirect('https://pitangui.amazon.com/api/skill/link/M2005KTMHCZ024?state=' + body.state + '&code=' + alexaUser.session_id);                         
              }
                      })
                      .catch(function(err) {
                          logger.error(err.message);
                          body.title = "B One";
                          body.message = "Somthing went wrong, please try once again.";
                          res.render('index.html', body);
                      });

                } else {
                    //TODO: Updating the user as per cusrrent scenario, if this user as old user.
                    logger.trace('Login success alexaUserArray 0.' + JSON.stringify(alexaUser)+ " invalid session_id"+alexaUser.session_id);
                    that.userHubModel.findAll({
                            where: {
                                user_id: user.user_id,
                                act_enabled: true
                            },
                            hub_id: {
                                $and: {
                                    $ne: null,
                                    $ne: ""
                                }
                            }
                        })
                        .then(function(hubs) {
                            if (hubs.length === 0) {
                                body.title = "B One";
                                body.message = "No hubs added to this account. Please add the hub and try again.";
                                res.render('index.html', body);
                            } else {
                                hub = hubs[0];

                                logger.trace("ELSE 2 HUB :" + JSON.stringify(hub));
                                var sessionId = User.generateSessionId();
                                code = user.user_id + "_"; //user_id in 0
                                code += sessionId + "_"; //sessionid in 1
                                logger.trace("ELSE HUB OBJ STATUS:" + (hub !== undefined && hub !== null));
                                if (hub !== undefined && hub !== null) {
                                    code += hub.hub_id; // hub_id in 2

                                    var _alexaUser = {
                                        user_id: user.user_id,
                                        state: body.state,
                                        client_id: body.client_id,
                                        scope: body.scope,
                                        response_type: body.response_type,
                                        session_id: code,
                                        email_id: user.email_id,
                                        access_token: code,
                                        count: count
                                    };

                                    alexaUser.update(_alexaUser)
                                        .then(function() {
                                            if (res) {
        //  res.redirect('https://pitangui.amazon.com/api/skill/link/M16NCQ995CQJFR?state=' + body.state + '&code=' + _alexaUser.session_id);
       res.redirect('https://pitangui.amazon.com/api/skill/link/M2005KTMHCZ024?state=' + body.state + '&code=' + _alexaUser.session_id);       
                                 }
                                        })
                                        .catch(function(err) {
                                            logger.error(err.message);
                                            body.title = "B One";
                                            body.message = "Somthing went wrong, please try once again.";
                                            res.render('index.html', body);
                                        });
                                }

                            }
                        })
                        .catch(function(err) {
                            body.title = "B One";
                            body.message = "Somthing went wrong, please try once again.";
                            res.render('index.html', body);
                        });
                }
            }
        })
        .catch(function(err) {
            logger.error(err.message);
            body.title = "B One";
            body.message = "Somthing went wrong, please try once again.";
            res.render('index.html', body);
        });
};

Auth.prototype.getModels = function(){
    if(this.userModel){
        return ;
    }
    var Models = db.getModels();
    this.userModel = Models.User;
    this.userHubModel = Models.UserHub;
    this.userDevModel = Models.UserDev;
    this.hubModel = Models.Hub;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.remoteModel = Models.RemoteKeyInfo;
    this.alexaUserModel =Models.AlexaUser;
};

module.exports = new Auth();
