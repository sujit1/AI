var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , Globals = require('../../globals')
    , log4js = require('log4js')
    , User = require('../User/Users')
    , csv = require("fast-csv");

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function DeviceCategory() {
    _.bindAll(this, "addDeviceCategory", "addSubCategory", "checkAndAddSubcategory", "getCategories", "deleteCategory"
        , "updateCategory", "loadCategoriesFromCsv", "getModels");
        // , "updateCategory", "loadCategoriesFromCsv", "getModels", "loadOldData");

    router.post('/addCategory', User.validateSession, this.addDeviceCategory);
    router.post('/addSubCategory', User.validateSession, this.addSubCategory);
    router.post('/getCategories', User.validateSession, this.getCategories);
    router.post('/deleteCategory', User.validateSession, this.deleteCategory);
    router.post('/updateCategory', User.validateSession, this.updateCategory);
    router.post('/loadCategoriesFromCsv', User.validateSession, this.loadCategoriesFromCsv);
    // router.post('/loadOldData', User.validateSession, this.loadOldData);

    return router;
};

DeviceCategory.prototype.addDeviceCategory = function(req, res) {
    logger.trace('addDeviceCategory Start: ' + JSON.stringify(req.body));
    var that = this
    , obj = {};
    obj.category_name = req.body.category_name;
    obj.category_desc = req.body.category_desc;
    obj.category_type = req.body.category_type;
    obj.category_view_type = req.body.category_view_type;
    obj.device_make = req.body.device_make;
    obj.device_model = req.body.device_model;
    obj.device_radio_type = req.body.device_radio_type;
    obj.category_parent_id = "";

    that.getModels();
    that.checkAndAddSubcategory(res, obj);
};

DeviceCategory.prototype.addSubCategory = function(req, res) {
    logger.trace('addSubCategory Start: ' + JSON.stringify(req.body));
    var that = this
    , parent = req.body.category_parent_id
    , obj = {};
    obj.category_name = req.body.category_name;
    obj.category_desc = req.body.category_desc;
    obj.category_view_type = req.body.category_view_type;
    obj.device_make = req.body.device_make;
    obj.device_model = req.body.device_model;
    obj.device_radio_type = req.body.device_radio_type;

    that.getModels();
    that.deviceCategoryModel.findAll({where: { category_id: parent }})
        .then(function(dev){
            if(dev.length === 0){
                res.send({status: 0, message: "No such main category present."});
                logger.trace('addSubCategory End');
            }else{
                obj.category_parent_id = dev[0].category_id;
                obj.category_type = dev[0].category_type;
                that.checkAndAddSubcategory(res, obj);
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
            logger.trace('addSubCategory End');
        });
};

DeviceCategory.prototype.checkAndAddSubcategory = function(res, obj) {
    logger.trace('checkAndAddSubcategory Start');
    var that = this;
    that.deviceCategoryModel.findAll({where: { category_name: obj.category_name }})
        .then(function(devSub){
            if(devSub.length === 0){
                that.deviceCategoryModel.build(obj).save()
                    .then(function(rec){
                        res.send({status: 1, message: "category added successfully."});
                        logger.trace('checkAndAddSubcategory End');
                    });
            }else{
                res.send({status: 0, message: "Category already exists."});
                logger.trace('checkAndAddSubcategory End');
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
            logger.trace('checkAndAddSubcategory End');
        });
};

// DeviceCategory.prototype.getCategories = function(req, res) {
//     logger.trace('getCategories Start: ' + JSON.stringify(req.body));
//     var that = this
//     , query = {}
//     query.category_type = req.body.reqObject.category_type;
//     query.category_parent_id = req.body.reqObject.parent_id;

//     that.getModels();
//     that.deviceCategoryModel.findAll({where: query})
//         .then(function(categories){
//             if(categories.length !== 0){
//                 res.send({status: 1, message: "All categories are in resObject.", resObject: { categories: categories }});
//                 logger.trace('getCategories End');
//             }else{
//                 res.send({status: 0, message: "No categories present. Please contact admin."});
//                 logger.trace('getCategories End');
//             }
//         })
//         .catch(function(err){
//             res.send({status: 0, message: err.message });
//             logger.trace('getCategories End');
//         });
// };

DeviceCategory.prototype.getCategories = function(req, res) {
    logger.trace('getCategories Start: ' + JSON.stringify(req.body));
    var that = this
    , query = {};
    query.category_type = req.body.reqObject.category_type;
    if(query.category_type === ""){
        query.category_type = {$in: ["Security" , "Remote" , "Music" , "Devices"]};
    }

    that.getModels();
    that.deviceCategoryModel.findAll({order: 'category_parent_id', where: query})
        .then(function(categories){
            if(categories.length !== 0){
                var parents = _.chain(categories)
                                .filter(function(v){return v.category_parent_id == ""})
                                .sortBy("category_name")
                                .value();
                var arr = [];
                for(var i in parents){
                    var subArr = _.chain(categories)
                                .filter(function(v){return v.category_parent_id == parents[i].category_id})
                                .sortBy("category_name")
                                .value();

                    parents[i].dataValues.childs = subArr;
                    arr.push(parents[i]);
                }
                res.send({status: 1, message: "All categories are in resObject.", resObject: { categories: arr }});
                logger.trace('getCategories End');
            }else{
                res.send({status: 0, message: "No categories present. Please contact admin."});
                logger.trace('getCategories End');
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
            logger.trace('getCategories End');
        });
};

DeviceCategory.prototype.deleteCategory = function(req, res) {
    logger.trace('deleteCategory Start: ' + JSON.stringify(req.body));
    var that = this
    , category_id = req.body.reqObject.category_id;

    that.getModels();
    that.deleteCategory.findAll({where: {$or: {category_parent_id: category_id, category_id: category_id}}})
        .then(function (categs) {
            if(categs.length != 0){
                var categsIds = _.pluck(categs, "category_id");
                that.deviceModel.findAll({where: {category_id: {$in: categsIds}, device_state: "ADDED"}})
                    .then(function(devs){
                        if(devs.length === 0){
                            that.deviceCategoryModel.destroy({where: {$or: {category_parent_id: category_id, category_id: category_id}}})
                                .then(function(categories){
                                    res.send({status: 1, message: "Category deleted successfully."});
                                    logger.trace('deleteCategory End');
                                });
                        }else{
                            res.send({status: 1, message: "There are devices added in this category. Please delete them and try deleting category."});
                            logger.trace('deleteCategory End');
                        }
                    });
            }else{
                res.send({status: 0, message: "No categories are present with that id."});
                logger.trace('deleteCategory End');
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

DeviceCategory.prototype.updateCategory = function(req, res) {
    logger.trace('updateCategory Start: ' + JSON.stringify(req.body));
    var that = this
    , category_id = req.body.reqObject.category_id
    , obj = {};
    obj.category_name = req.body.reqObject.category_name;
    obj.category_parent_id = req.body.reqObject.category_parent_id;
    obj.category_desc = req.body.reqObject.category_desc;
    obj.category_type = req.body.reqObject.category_type;
    obj.category_view_type = req.body.reqObject.category_view_type;
    obj.device_make = req.body.reqObject.device_make;
    obj.device_model = req.body.reqObject.device_model;
    obj.device_radio_type = req.body.reqObject.device_radio_type;

    that.getModels();
    that.deviceCategoryModel.update(obj, {where: {category_id: category_id}})
        .then(function(result){
            logger.trace(result);
            if(result === 0){
                res.send({status: 0, message: "No category is present with the given id."});
                logger.trace("updateCategory End");
            }else{
                res.send({status: 1, message: "Category updated successfully."});
                logger.trace("updateCategory End");
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

DeviceCategory.prototype.loadCategoriesFromCsv = function(req, res){
    logger.trace('loadCategoriesFromCsv Start');
    var that = this
    , arr = [];

    that.getModels();
    csv
     .fromPath(Globals.appRoot + '/Db/devices.csv', {headers: true}, function(error){
        res.send({status: 0, message: "No devices file is present to upload. Please contact admin."});
        logger.trace('loadCategoriesFromCsv End');
     })
     .on("data", function(data){
        arr.push(data);
     })
     .on("end", function(){
         if(arr.length === 0){
            res.send({status: 0, message: "No records to upload." });
            logger.trace('loadCategoriesFromCsv End');
            return;
         }
         that.deviceCategoryModel.bulkCreate(arr)
            .then(function(recs){
                that.deviceCategoryModel.findAll({where: {category_parent_id: ""}})
                    .then(function(catRecs){
                        catRecs.forEach(function(rec){
                            that.deviceCategoryModel.update({category_parent_id: rec.dataValues.category_id}, {where: {category_parent_id: rec.dataValues.category_name}});
                        });
                    })
                if(recs !== 0){
                    res.send({status: 1, message: "Loading categories done." });
                    logger.trace('loadCategoriesFromCsv End');
                }else{
                    res.send({status: 0, message: "No records are uploaded" });
                    logger.trace('loadCategoriesFromCsv End');
                }
            })
            .catch(function(err){
                res.send({status: 0, message: err.message });
                logger.trace('loadCategoriesFromCsv End');
            });
     });
}

DeviceCategory.prototype.getModels = function(){
    if(this.deviceCategoryModel){
        return ;
    }
    var Models = db.getModels();
    this.deviceCategoryModel = Models.DeviceCategory;
    this.deviceModel = Models.Devices;
};

module.exports = new DeviceCategory();