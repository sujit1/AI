var express = require('express')
    , router = express.Router()
    , crypto = require('crypto')
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , Globals = require('../../globals')
    , redisDb = require('../../redisDB')
    , mongodb = require('../../db')
    , QueueHandler = require('../Commons/QueueHandler')
    , log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function CustomerCare() {
    _.bindAll(this, "getCustomerCareUsers", "addCustomerCareUser", "deleteCustomerCareUsers", 'loginUser', 'logout', 'getUserWithPin'
            , 'saveTicket', 'closeTicket', 'getTickets');
                    
    router.post('/login', this.loginUser);
    router.post('/addUser', this.addCustomerCareUser);
    router.post('/logout', this.logout);
    router.get('/getUsers', this.getCustomerCareUsers);
    router.post('/deleteUser', this.deleteCustomerCareUsers);
    router.post('/getUserWithPin', this.getUserWithPin);
    router.post('/saveTicket', this.saveTicket);
    router.post('/closeTicket', this.closeTicket);
    router.post('/getTickets', this.getTickets);

    this.userCollection = 'customerCareUser';
    this.ticketCollection = 'customerCareTickets';

    return router;
};

CustomerCare.prototype.addCustomerCareUser = function(req, res) {
    logger.trace('addCustomerCareUser Start: ' + JSON.stringify(req.body));
    var that = this
        , obj = {
                user_id: req.body.user_id,
                password: that.encryptPassword(req.body.password),
                act_enabled: true,
                timestamp: new Date()
            };
    mongodb.findByObject(that.userCollection, {user_id: obj.user_id}, function(err, data){
        if(err){
            logger.info("some problem in saving details: " + err);
            res.send({status: 0, message: "some problem in saving details: " + err});
        }else{
            if(data){
                if(data.act_enabled){
                    res.send({status: 0, message: "User already exist."});
                }else{
                    mongodb.update(that.userCollection, data._id, obj, function(err, _data){
                        if(err){
                            logger.info("some problem in saving details: " + err);
                            res.send({status: 0, message: "some problem in saving details: " + err});
                        }else{
                            res.send({status: 1, message: "User created successfully."});
                        }
                    });
                }
            }else{
                mongodb.save(that.userCollection, obj, function(err, data){
                    if(err){
                        logger.info("some problem in saving details: " + err);
                        res.send({status: 0, message: "some problem in saving details: " + err});
                    }else{
                        res.send({status: 1, message: "User created successfully."});
                    }
                });
            }
        }
    });
    logger.trace('addCustomerCareUser End');
};

CustomerCare.prototype.getCustomerCareUsers = function(req, res) {
    logger.trace('getCustomerCareUsers Start: ' + JSON.stringify(req.body));
    var that = this;
    mongodb.findByObjects(that.userCollection, {act_enabled: true}, function(err, data){
        if(err){
            logger.info("some problem in saving details: " + err);
            res.send({status: 0, message: "some problem in saving details: " + err});
        }else{
            var _data = _.chain(data)
                            .map(function(currentObject) {
                                var i = _.pick(currentObject, 'user_id', '_id');
                                if(i.user_id != 'superadmin'){
                                    return i;
                                }
                            })
                            .compact()
                            .value();
            res.send({status: 1, message: "User retrived successfully.", users: _data});
        }
    });
    logger.trace('getCustomerCareUsers End');
};

CustomerCare.prototype.deleteCustomerCareUsers = function(req, res) {
    logger.trace('deleteCustomerCareUsers Start: ' + JSON.stringify(req.body));
    var that = this;
    mongodb.update(that.userCollection, req.body.id, {act_enabled: false}, function(err, data){
        if(err){
            logger.info("some problem in saving details: " + err);
            res.send({status: 0, message: "some problem in saving details: " + err});
        }else{
            logger.info(JSON.stringify(data));
            res.send({status: 1, message: "User deleted successfully."});
        }
    });
    logger.trace('deleteCustomerCareUsers End');
};

CustomerCare.prototype.loginUser = function(req, res) {
    logger.trace('loginUser Start: ' + JSON.stringify(req.body));
    var that = this;
    mongodb.findByObjects(that.userCollection, {user_id: req.body.user_id, password: that.encryptPassword(req.body.password), act_enabled: true}, function(err, data){
        if(err){
            logger.info("some problem in saving details: " + err);
            res.send({status: 0, message: "some problem in saving details: " + err});
        }else{
            if (data.length !== 0) {
                var sessionId = that.generateSessionId();
                // mongodb.update('customerCareSession', {user_id})
                res.send({status: 1, message: "Login success.", sessionId: sessionId});
            }else{
                res.send({status: 0, message: "Login failed. No user exist with the given combination."});
            }
        }
    });
    logger.trace('loginUser End');
};

CustomerCare.prototype.logout = function(req, res) {
    logger.trace('logout Start: ' + JSON.stringify(req.body));
    var that = this;
    mongodb.findByObjects(that.userCollection, {user_id: req.body.user_id, password: that.encryptPassword(req.body.password), act_enabled: true}, function(err, data){
        if(err){
            logger.info("some problem in saving details: " + err);
            res.send({status: 0, message: "some problem in saving details: " + err});
        }else{
            if (data.length !== 0) {
                // mongodb.update('customerCareSession', {user_id})
                res.send({status: 1, message: "Logout success."});
            }else{
                res.send({status: 0, message: "No user exist with the given combination."});
            }
        }
    });
    logger.trace('logout End');
};

CustomerCare.prototype.saveTicket = function(req, res){
    this.modifyTicket(req, res, false);
};

CustomerCare.prototype.closeTicket = function(req, res){
    this.modifyTicket(req, res, true);
};

CustomerCare.prototype.modifyTicket = function(req, res, bool) {
    var that = this
        , obj = req.body.reqObject
        , ticketId = req.body.ticketId
        , customer_pin = req.body.customer_pin
        , email_id = req.body.email;

    var date = new Date;
    obj.isPending = req.body.isPending;
    if(obj.hasOwnProperty('isPending') && !obj.isPending){
        obj.fullDate = that.getFullDate(date) + " " + that.getFullTime(date);
    }
    logger.trace("modifyTicket: " + JSON.stringify(req.body));
    mongodb.updateByQuery(that.ticketCollection, { ticketId: ticketId, customer_pin: customer_pin }, obj, function(err, data){
        if(err){
            logger.info("Something happend. Please contact admin." + err);
            res.send({status: 0, message: "Something happend. Please contact admin."});
        }else{
            if(bool){
                logger.info("Closed ticket with " + obj.status + ".");
                res.send({status: 1, message: "Closed ticket with " + obj.status + "."});
                QueueHandler.sendMail({
                    to: email_id, // list of receivers
                    subject: Globals.MailerReportSubject, // Subject line
                    templateData: { ticketId: ticketId, logo: 'http://' + req.headers.host + "" + Globals.MailLogo, date: obj.fullDate , greeting: "Dear Sir/Madam" },
                    templateName: (obj.isPending ? Globals.template.created : Globals.template.resolved)
                });
            }else{
                res.send({status: 1, message: "Ticket Saved."});
            }
        }
    });
};

CustomerCare.prototype.getFullDate = function(date) {
    return ("00" + date.getDate()).slice(-2) + "/" + ("00" + (date.getMonth()+1)).slice(-2) + "/" + date.getFullYear();
};

CustomerCare.prototype.getFullTime = function(date) {
    var hours = date.getHours(), minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    return ("00" + hours).slice(-2) + ":" + ("00" + minutes).slice(-2) + ":" + ("00" + date.getSeconds()).slice(-2) + ' ' + ampm;
    // return ("00" + hours).slice(-2) + ":" + ("00" + minutes).slice(-2) + ":" + ("00" + date.getSeconds()).slice(-2);
};

CustomerCare.prototype.getUserWithPin = function(req, res) {
    var that = this
        , pin = req.body.customer_pin;
    that.getModels();
    that.userModel.findAll({where: {customer_pin: pin}})
        .then(function(data){
            if(data.length === 0){
                res.send({status: 0, message: "No user exist."});
            }else{
                var date = new Date();
                var obj = {
                    customer_pin: pin,
                    email: data[0].email_id,
                    user_id: data[0].user_id,
                    status: "Pending",
                    isPending: true,
                    timestamp: that.getFullDate(date) + " " + that.getFullTime(date)
                };
                mongodb.findAll(that.ticketCollection, function(err, _data){
                    if(err){
                        res.send({status: 0, message: "Something happend. Please contact admin."});
                    }else{
                        //if(_data.length) //TODO: need to work on resetting of collection on reaching max count.
                        var ticketId = ("000000" + (_data.length + 1)).slice(-6);
                        obj.ticketId = ticketId;
                        mongodb.save(that.ticketCollection, obj, function(err, _result){
                            if(err){
                                res.send({status: 0, message: "Something happend. Please contact admin."});
                            }else{
                                res.send({status: 1, message: "Got the user.", user: obj});
                            }
                        });
                    }
                });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

CustomerCare.prototype.getTickets = function(req, res) {
    var that = this;
    mongodb.findSortedObjects(that.ticketCollection, {}, {timestamp: -1}, function(err, _data){
        if(err){
            res.send({status: 0, message: "Something happend. Please contact admin."});
        }else{
            // res.setHeader('content-type', 'text/javascript');
            // TODO: sending data in Gzip format.
            res.send({status: 1, message: "Got the user.", _data: _data});
        }
    });
};

CustomerCare.prototype.encryptPassword = function(id) {
    var sha = crypto.createHash('md5');
    sha.update(id);
    return sha.digest('hex');
};

CustomerCare.prototype.generateSessionId = function() {
    var sha = crypto.createHash('sha1');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

CustomerCare.prototype.getModels = function(){
    if(this.userModel){
        return ;
    }
    var Models = db.getModels();
    this.hubModel = Models.Hub;
    this.userHubModel = Models.UserHub;
    this.userModel = Models.User;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.deviceCategoryModel = Models.DeviceCategory;
};

module.exports = new CustomerCare();