var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , Globals = require('../../globals')
    , QueueHandler = require('../Commons/QueueHandler')
    , mysqlDb = require('../../mySqlDB')
    , db = require('../../db')
    , crypto = require('crypto')
    , User = require('../User/Users')
    , Globals = require('../../globals')
    , log4js = require('log4js')
    , request = require("request")
    , Event= require('../Events/Events')
    , lsDevice=require('../Device/Device')
    , line = require('@line/bot-sdk');


    const config = {
    channelAccessToken:'GDOLGAXMKk2v/KbIuLk7fqcK8HFe6EneL8Lw6WyvqK58TC3EyZg17vmxQnY8juggQ0NiCW6HtW2yWVpHBHQGFJYGdHRhggtR9J7koDTyQT2i7jPwiM9khRbdkNyJH/I3swFhUUgMAnzmj1d8X36bFgdB04t89/1O/w1cDnyilFU=',//process.env.CHANNEL_ACCESS_TOKEN,
    channelSecret:'814625b04c9c66c6a96f13bd3685f9c7', // process.env.CHANNEL_SECRET,
  };

// create LINE SDK client
const client = new line.Client(config);


const createSignature = (secret, body) => {
  return crypto
    .createHmac('sha256', secret)
    .update(body)
    .digest('base64');
};


log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Webhook() {
    _.bindAll(this, "webhookfunction","checkSignature","getModels","securityCall","postBackcall","sensorCall"
    ,"ActionCall","LocksCall","MenuCall","showDeviceStatus","showActionStatus","CameraCall","ByeCall","showCameraStatus"
     ,"postLogin" ,"getLogin","encryptPassword","Lightscall","showPhilpseStatus","besimage","showSwitchStatus");

    router.post('/webhook',this.checkSignature);
    router.post('/lineNotify',this.pushNotify);
    router.post('/login', this.postLogin);
    router.get('/login', this.getLogin);
    router.get('/besimage', this.besimage);


    return router;
};

Webhook.prototype.pushNotify = function(req, res) {
  line.middleware(config);
  var userId=req.body.lineId;
  var echo={ type: 'text', text: req.body.message };
var msg;



   var msgcame=req.body.message.split(":");
        if(msgcame.length<5)
        {

           if(msdcame[3]=="ARM"|| msgcame[3]=="DISARM" || msgcame[3] == "INHOUSE")
           {
              msg="LSモード set to " + "\"" + msdcame[3] + " for " + msdcame[0];
           }
           else if(msgcame.length>0){
             msg= msdcame[2] + " " + "\"" + msdcame[3] +  "\"" + " in " + msdcame[1] + " of " + msdcame[0] ;
           }
           else {
             msg=req.body.message;
           }

         }
         else {

            msg=msdcame[2] + " " + "\"" + msdcame[3] +  "\"" + " in " + msdcame[1] + " of " + msdcame[0]  +"beseyeapp://liveView";
         }




  var echo={ type: 'text', text: msg };
  logger.info("comming to the notification :"+JSON.stringify(echo));
  res.send({status: 1, message: "data receied sucessfully"});
  client.pushMessage(userId, echo);
};


Webhook.prototype.checkSignature = function(req, res) {
  line.middleware(config);
  this.webhookfunction(req,res);
};

Webhook.prototype.webhookfunction = function(req, res) {
    var that = this;


Promise
.all(req.body.events.map(function(event){
  that.handleEvent(event);

}))


};



Webhook.prototype.handleEvent=function (event) {
    var that=this;
  //  variable declaration
  var hub_id,user_id,echo;
    //printing the event request
    logger.info("event comming here   ::: "+JSON.stringify(event));
    //get the line user id
    var lineId=event.source.userId;
    var atpos,dotpos,x;

   // checkigng the Line user

     that.getModels();
     // email condition check
       if(event.type === "message")
       {
         x=event.message.text.toString();
         atpos = -1;//x.indexOf("@");
         dotpos = -1;//x.lastIndexOf(".");
       }
       else {
         atpos=-1;
         dotpos=-1;
       }



      if(atpos === -1 && dotpos === -1)
      {
        // if(event.type === "message" && (event.message.text==="Hi" || event.message.text==="Hello" || event.message.text==="my home" || event.message.text==="Hi. What do I do next" ))
        // {
        //   //echo={ type: 'text', text: "Hi.  Thank you for adding me to your friend list. Please type your LS email id." };
        //   echo={ type: 'text', text: "こんにちは。友達になってくれてありがとう！, LS ID（メールアドレス）を入力して下さい" };
        //   logger.error("data sending :" + JSON.stringify(echo));
        //   return client.replyMessage(event.replyToken, echo);
        //
        // }
        // else {


        that.lineModel.findAll({where: {line_id: lineId}})
                        .then(function(user){

                          if(user.length>0)
                          {
                            hub_id=user[0].hub_id;
                            user_id=user[0].user_id;
                            //echo={ type: 'text', text: "you have conformed the email id " };
                           //  logger.error("data sending :" + JSON.stringify(echo));
                            //return client.replyMessage(event.replyToken, echo);

                              if(event.type === "message" && (event.message.text==="LSモード" || event.message.text==="I want to check the security details of my house"))
                              {
                                 that.securityCall(user_id,hub_id,event.replyToken);
                              }
                               if(event.type === 'postback')
                              {

                                 that.postBackcall(user_id,hub_id,event.postback.data,event.replyToken)
                              }

                              if(event.type === "message" && event.message.text==="デバイス" )
                              {
                                that.sensorCall(user_id,hub_id,event.replyToken);
                              }

                              if(event.type === "message" && event.message.text==="メニュー" )
                              {

                                that.MenuCall(event.replyToken,lineId);
                              }
                              if(event.type === "message" && event.message.text==="コマンド" )
                              {
                                that.ActionCall(user_id,hub_id,event.replyToken);
                              }

                              if(event.type === "message" && event.message.text==="スマートロック" )
                              {

                                that.LocksCall(user_id,hub_id,event.replyToken);
                              }
                              if(event.type === "message" && event.message.text==="監視カメラ" )
                              {
                                that.CameraCall(user_id,hub_id,event.replyToken);
                              }
                              if(event.type === "message" && (event.message.text==="bye"||event.message.text==="Bye" || event.message.text==="バイ"|| event.message.text==="Good Bye")  )
                              {
                                that.ByeCall(user_id,hub_id,lineId,event.replyToken);
                              }
                              if(event.type === "message" && (event.message.text==="Thanks"|| event.message.text==="Thank you" ||event.message.text=== "Thank You"|| event.message.text==="ThankYou"||event.message.text==="ok thanks") )
                              {
                                //echo = { type: 'text', text: "You are welcome.Please visit LS dashboard to see other features."};
                                echo = { type: 'text', text: "こちらこそ。その他の機能はLSダッシュボードか"};
                                return client.replyMessage(event.replyToken, echo);
                              }
                              if(event.type === "message" && event.message.text==="照明" )
                              {
                                that.Lightscall(user_id,hub_id,event.replyToken);
                              }




                          }
                          else {
                          //   echo={ type: 'text', text: "Please type your LS email id." };
                          echo = {
                                    "type": "template",
                                    "altText": "Live Smart ",
                                    "template": {
                                       "type": "buttons",
                                       "title": "Emailを登録する",
                                       "text": "LS Email IDを入力して下さい",
                                       "actions": [
                                                     {
                                                       "type": "uri",
                                                       "label": "ログインして下さい",
                                                       "uri": "https://line.b1automation.com/webhook/login?state="+lineId
                                                     }

                                                  ]
                                                }
                                    };
                             logger.error("data sending :" + JSON.stringify(echo));
                             return client.replyMessage(event.replyToken, echo);
                          }

                        });

        //   }
      //end of the email check
      }
      else {

        that.userModel.findAll({where: {email_id:x} })
          .then(function (arr) {

              if(arr.length>0)
              {
                   user_id=arr[0].user_id;
                   user_pwd=arr[0].
                   logger.info("user id is :"+user_id);
                   that.userHubModel.findAll({where: {user_id:user_id} })
                     .then(function (userhub) {
                      hub_id=userhub[0].hub_id;

                      that.lineModel.build({user_id: user_id, hub_id: hub_id,line_id : lineId,email_id:x}).save()
                          .then(function(){

                            //that.securityCall(user_id,hub_id,event.replyToken);
                            //echo={ type: 'text', text: "Thank you for verifying your email address. Start using the dashboard, or ask me about your LS."};
                            echo={ type: 'text', text: "メールアドレスの認証ありがとうございます,まずダッシュボードから始めてみましょう、またはLSについて私に聞いて下さい"};
                            logger.error("data sending :" + JSON.stringify(echo));
                            return client.replyMessage(event.replyToken, echo);
                          });

                     });


              }
              else {

                echo = {
                          "type": "template",
                          "altText": "Live Smart ",
                          "template": {
                             "type": "buttons",
                             "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
                            // "title": "Email ID is not registered",
                             //"text": "Register your email id on LS App",
                             "title": "EメールIDが登録されていません",
                             "text": "LSアプリでEメールIDを登録して下さい",
                             "actions": [
                                           {
                                             "type": "uri",
                                             "label": "Android",
                                             "uri": "https://www.livesmart.jp/"
                                           },
                                           {
                                             "type": "uri",
                                             "label": "Ios",
                                             "uri": "https://www.livesmart.jp/"
                                           }
                                        ]
                                      }
                          };
                logger.error("data sending :" + JSON.stringify(echo));
                return client.replyMessage(event.replyToken, echo);

              }

          });

      //end of else condition of email check
      }


};


//security call

//security call

Webhook.prototype.securityCall = function(user_id,hub_id,rToken){
var that =this;
var timestamp;
var tmbImg;
var msg;
var msgtext;
var label1,label2;
var act1,act2;
 that.getModels();
 var query={"hub_id":hub_id,"device_b_one_id":"0000"};
 db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
     logger.error("sujit test"+ JSON.stringify(data));
             //  msg=""+data[0].security_mode;
          timestamp=" "+data[0].security_timestamp;
          logger.info(""+timestamp);
           msgtext="以下から変更したいステータスを選んで下さい";
                switch(data[0].security_mode)
                 {
                         case "0":
                         case "DISARM":   msg="ハウスステータス：セキュリティ解除";
                                   label1="セキュリティ強化";
                                   label2="在宅";
                                   act1="arm";
                                   act2="inhouse";
                                   tmbImg="https://line.b1automation.com/images/disarmed.png";
                                   break;
                         case  "1":
                         case  "ARM":            msg="ハウスステータス：セキュリティ強化";
                                   label1="セキュリティ解除";
                                   label2="在宅";
                                   act1="disarm";
                                   act2="inhouse";
                                   tmbImg="https://line.b1automation.com/images/armed.png";
                                   break;
                        case  "3":
                        case  "ARM ALERT":
                                  msg="ハウスステータス：セキュリティ強化";
                                 label1="セキュリティ解除";
                                 label2="在宅";
                                  act1="disarm";
                                  act2="inhouse";
                                  tmbImg="https://line.b1automation.com/images/alert.png";
                                  break;

                         case "2":
                         case "INHOUSE":           msg="ハウスステータス：在宅";
                          label1="セキュリティ強化";
                          label2="セキュリティ解除";
                                   act1="arm";
                                   act2="disarm";
                                   tmbImg="https://line.b1automation.com/images/inhouse.png";

                                   break;

                                   case "4":
                                  case "INHOUSE ALERT":
                                  msg="ハウスステータス：在宅N";
                                  label1="セキュリティ強化";
                                  label2="セキュリティ解除";
                                           act1="arm";
                                           act2="disarm";
                                            tmbImg="https://line.b1automation.com/images/ihalert.png";

                                            break;



                 }

                         echo= {
                                    "type": "template",
                                    "altText": "Live Smart ",
                                    "template": {
                                     "type": "buttons",
                                     "thumbnailImageUrl": tmbImg,
                                     "title": msg+timestamp,
                                     "text":msgtext,
                                     "actions": [
                                         {
                                           "type": "postback",
                                           "label": label1,
                                           "data": act1
                                         },
                                         {
                                           "type": "postback",
                                           "label": label2,
                                           "data": act2
                                         }
                                     ]
                                    }
                                    };


            logger.info("echo msg ::"+ JSON.stringify(echo));

            return client.replyMessage(rToken, echo);
},1);


};


//sensor Call
Webhook.prototype.sensorCall = function(user_id,hub_id,rToken){

var query;
  var options = {
                        url: "https://line.b1automation.com/device/getDevices",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:{"hub_id":hub_id}
                 };

                 request(options, function(err, res, resbody){

          if(!err)
          {

           //logger.info("data from server :"+JSON.stringify(resbody));

                if(resbody.status=== 0)
                {
                  echo = { type: 'text', text: resbody.message };
                   logger.info("data from server :"+JSON.stringify(echo));
                  return client.replyMessage(rToken, echo);
                }
                else {

                     var deviceData=resbody.devices;
                     var requiredData=[];
                    logger.error("count of devices"+ deviceData.length );
                     _.each(deviceData,function(v){
   if(v.category_id == "7643a31c-e4f9-4b34-a646-c9a9125487c9"|| v.category_id == "c3d989f4-0080-4ecc-a9be-ae90ba4c0363" || v.category_id == "3f695ece-9b66-40ee-a80f-c9785468428f" ||  v.category_id == "328f9632-d0b8-4bce-a572-6d9348ddec89" || v.category_id == "0c66ae21-f630-4373-ae95-223cd955a6a0" || v.category_id == "78eec792-fe66-41c7-be0f-ff1e958d4b46" || v.category_id == "36bcaaa5-342e-4209-ad49-542bec9e3f38" || v.category_id == "ca9c0678-708e-4961-8a95-98b81d4fa580" || v.category_id == "44da5b21-ec96-4669-98a6-2c1e26a8f503" || v.category_id == "f0609d16-e3cc-46ab-87a5-4427563fe47b" || v.category_id =="31d39871-cbae-44a2-863e-08007075213b")
                             {
                                requiredData.push(v);
                             }


                     });
                     logger.error("final device count"+ requiredData.length );
                     if(requiredData.length>0)
                     {
                       var i = 0;
                       var actiondata=[];
                       var actionArraySet=[];
                       var columnData=[];

                         _.each(requiredData,function(rec) {
                            var b={};
                            b.type="postback";
                            b.label=rec.device_name;//+"-"+rec.room_name;
                            b.data= rec.device_b_one_id+"_"+rec.category_id;
                            actiondata.push(b);
                            i++;
                            if(i%3 ==0){
                              var actionArray=[];
                              actionArray.push(actiondata[i-1]);
                              actionArray.push(actiondata[i-2]);
                              actionArray.push(actiondata[i-3]);
                              actionArraySet.push(actionArray);
                            }
                         });


                         var remining=(i%3);
                         var emptyCount=3-remining;

                          if(remining>0)
                          {
                           var actionArray=[];
                           actionArray.push(actiondata[i-1]);
                           remining--;
                           if(remining>0)
                           actionArray.push(actiondata[i-2]);

                           while(emptyCount>0)
                           {

                                  var b={};
                                  b.type="uri";
                                  b.label="  ";//+" - "+rec.room_name;
                                  b.uri= "http://example.com/page/222";
                                  actionArray.push(b);
                                  emptyCount--;

                           }

                           actionArraySet.push(actionArray);
                          }



                            var counter=0;


                            _.each(actionArraySet,function(rec) {
                             counter++;
                               if(counter<=5)
                               {
                                var c={};
                               c.title="デバイス";
                               c.text="デバイスを選択して下さい";
                               c.actions=rec;
                               columnData.push(c);
                              }


                            });




                            //logger.error("device info"+JSON.stringify(actiondata));
                            echo= {
                                       "type": "template",
                                       "altText": "Live Smart ",
                                           "template": {
                                            "type": "carousel",
                                            "columns":columnData

                                             }
                                    };





                     logger.info("echo msg ::"+ JSON.stringify(echo));

                     return client.replyMessage(rToken, echo);



                     }
                     else {
                       //echo = { type: 'text', text: "Sorry, i did not find any sensors in your LS account" };
                       echo = { type: 'text', text: "デバイスが見つかりません" };
                       return client.replyMessage(rToken, echo);
                     }


                }


          }

         });


};


//postBack call

Webhook.prototype.postBackcall = function(user_id,hub_id,actionid,rToken){

var that=this;
var device_id;
var evtObject;
var msg;

if(actionid == "arm" || actionid == "disarm" || actionid == "inhouse")
{
  var query={"hub_id":hub_id,"device_b_one_id":"0000"};
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
      logger.error("sujit test"+ JSON.stringify(data));
              //  msg=""+data[0].security_mode;
           device_id=data[0].device_id;
          logger.info(""+device_id);



  switch(actionid)
                  {
                          case "arm":
                          case "ARM":    msg="セキュリティ強化";//msg="House is Armed!";ハウスステータス：
                     evtObject=   {
                                    "security_mode":"1",

                                   };
                                    break;
                          case  "disarm":
                          case  "DISARM": msg="セキュリティ解除";//msg="House is Disarmed! ";
                                          evtObject=   {
                                    "security_mode":"0",

                                   };

                                    break;

                          case "inhouse":
                          case "INHOUSE":      msg="在宅";     //msg="In-House! ";
                                   evtObject=   {
                                    "security_mode":"2",

                                   };

                                    break;
                        default :


                  }


          var jsondata={

  "hub_id":hub_id,
  "device_b_one_id":"0000",
  "device_id":device_id,
  "origin_id":1,
  "reqObject":evtObject
  }
               logger.info("json data is"+JSON.stringify(jsondata));

  var options = {
                       url: "https://line.b1automation.com/event/sendEvent",
                       method: 'POST',
                       port:443,
                       headers: {
                         'Content-Type': 'application/json',
                         'Accept':'application/json',

                       },
                       json:jsondata
                };

        request(options, function(err, res, resbody){

  if(!err)
  {
    if(res.statusCode == 401)
    {
         logger.info("access token changed");
    }
    else {


  logger.info("data upadted sucessfully"+JSON.stringify(resbody));
    echo = { type: 'text', text: "ハウスのセキュリティが強化"+msg +" その他はダッシュボード/LSモードへ。"};
    //  echo = { type: 'text', text: msg +" To view more, click on Dashboard/LS Mode"};
     return client.replyMessage(rToken, echo);

  }
  }

  });

  },1);
}
else
{


  var dtslice=actionid.split('_');

              switch(dtslice[1])
              {

                        case "7643a31c-e4f9-4b34-a646-c9a9125487c9":
                        case "3f695ece-9b66-40ee-a80f-c9785468428f":
                        case "328f9632-d0b8-4bce-a572-6d9348ddec89":
                        case "31d39871-cbae-44a2-863e-08007075213b":
                         //door sensor
                            that.showDeviceStatus(dtslice[0],hub_id,"door",rToken);

                        break;

                        case "c3d989f4-0080-4ecc-a9be-ae90ba4c0363":
                         // motion sensor
                           that.showDeviceStatus(dtslice[0],hub_id,"multi",rToken);

                        break;

                        case "91f98abb-7e90-40fa-ae46-8c54c1bbd563":
                         // actions

                           that.showActionStatus(dtslice[0],hub_id,"action",rToken,dtslice[2])

                        break;

                        case "7c076a49-43d7-4e26-a66f-a4b73142d06e":
                        // locks
                            that.showActionStatus(dtslice[0],hub_id,"lock",rToken,"null")
                        break;

                        case "251b4980-02ca-44f4-8331-219896bf72cf":
                             that.showCameraStatus(dtslice[0],hub_id,"cam",rToken,"null")

                        break;
                        case "0c66ae21-f630-4373-ae95-223cd955a6a0":
                             that.showCameraStatus(dtslice[0],hub_id,"cree",rToken,"null")

                        break;
                        case "ca9c0678-708e-4961-8a95-98b81d4fa580":
                         that.showPhilpseStatus(dtslice[0],hub_id,"group",rToken,"null")

                        break;
                        case "78eec792-fe66-41c7-be0f-ff1e958d4b46":
                        case "36bcaaa5-342e-4209-ad49-542bec9e3f38":

                             that.showPhilpseStatus(dtslice[0],hub_id,"bulb",rToken,"null")


                        break;
                        case "44da5b21-ec96-4669-98a6-2c1e26a8f503":
                              that.showSwitchStatus(dtslice[0],hub_id,"switch",rToken,"null");
                        break;
                        case "f0609d16-e3cc-46ab-87a5-4427563fe47b":
                              that.showSwitchStatus(dtslice[0],hub_id,"dimmer",rToken,"null");
                        break;
                        case "24a4d165-f524-4943-ab99-9a5e9a9bf3f4":

                             that.showNinjaStatus(dtslice[0],hub_id,"ninja",rToken,"null")
                         break;


                        case "enable":
                         // action enable
                         var evtObject= {
                              "routine_status":"1"
                         };
                         var jsondata={

                                  "hub_id":hub_id,
                                  "device_b_one_id":dtslice[0],
                                  "device_id":dtslice[2],
                                  "origin_id":1,
                                  "reqObject":evtObject
                                 }
                              logger.info("json data is"+JSON.stringify(jsondata));

                var options = {
                                      url: "https://line.b1automation.com/event/sendEvent",
                                      method: 'POST',
                                      port:443,
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept':'application/json',

                                      },
                                      json:jsondata
                               };

                       request(options, function(err, res, resbody){

                if(!err)
                {
                   if(res.statusCode == 401)
                   {
                        logger.info("access token changed");
                   }
                   else {


               logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                echo = { type: 'text', text: "コマンド  を有効にする。その他はダッシュボード/コマンドへ。"};
                  // echo = { type: 'text', text: "Your command is enabled,To view more, click on Dashboard/Commands"};
                    return client.replyMessage(rToken, echo);

                 }
                }

               });


                        break;

                        case "disable":
                         //action disable

                         var evtObject= {
                              "routine_status":"0"
                         };
                         var jsondata={

                                  "hub_id":hub_id,
                                  "device_b_one_id":dtslice[0],
                                  "device_id":dtslice[2],
                                  "origin_id":1,
                                  "reqObject":evtObject
                                 }
                              logger.info("json data is"+JSON.stringify(jsondata));

                var options = {
                                      url: "https://line.b1automation.com/event/sendEvent",
                                      method: 'POST',
                                      port:443,
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept':'application/json',

                                      },
                                      json:jsondata
                               };

                       request(options, function(err, res, resbody){

                if(!err)
                {
                   if(res.statusCode == 401)
                   {
                        logger.info("access token changed");
                   }
                   else {


               logger.info("data upadted sucessfully"+JSON.stringify(resbody));
               echo = { type: 'text', text: "コマンド は無効です。その他はダッシュボード/コマンドへ。" };
                   //echo = { type: 'text', text: "Your command is disabled,To view more, click on Dashboard/Commands" };
                    return client.replyMessage(rToken, echo);

                 }
                }

               });


                        break;

                        case "ninjaon":
                        //lock
                        var evtObject= {
                             "status":"2"
                        };
                        var jsondata={

                                 "hub_id":hub_id,
                                 "device_b_one_id":dtslice[0],
                                 "device_id":dtslice[2],
                                 "origin_id":1,
                                 "reqObject":evtObject
                                }
                             logger.info("json lock on 1 data is"+JSON.stringify(jsondata));

               var options = {
                                     url: "https://line.b1automation.com/event/sendEvent",
                                     method: 'POST',
                                     port:443,
                                     headers: {
                                       'Content-Type': 'application/json',
                                       'Accept':'application/json',

                                     },
                                     json:jsondata
                              };

                      request(options, function(err, res, resbody){

               if(!err)
               {
                  if(res.statusCode == 401)
                  {
                       logger.info("access token changed");
                  }
                  else {


              logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                echo = { type: 'text', text: "スマートロックは閉まっています 施錠するためにはダッシュボードからスマートロックをタップして下さい" };
                //  echo = { type: 'text', text: "Your lock is now unlocked,To turn lock, click on Dashboard/Locks" };
                   return client.replyMessage(rToken, echo);

                }
               }

              });


                        break;

                        case "ninjaoff":
                         // unlock
                         var evtObject= {
                              "status":"3"
                         };
                         var jsondata={

                                  "hub_id":hub_id,
                                  "device_b_one_id":dtslice[0],
                                  "device_id":dtslice[2],
                                  "origin_id":1,
                                  "reqObject":evtObject
                                 }
                              logger.info("json data is"+JSON.stringify(jsondata));

                var options = {
                                      url: "https://line.b1automation.com/event/sendEvent",
                                      method: 'POST',
                                      port:443,
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept':'application/json',

                                      },
                                      json:jsondata
                               };

                       request(options, function(err, res, resbody){

                if(!err)
                {
                   if(res.statusCode == 401)
                   {
                        logger.info("access token changed");
                   }
                   else {


               logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                  echo = { type: 'text', text: "スマートロックは開きました 施錠するためにはダッシュボードからスマートロックをタップして下さい" };
                  // echo = { type: 'text', text: "Your lock is now locked,To turn unlock, click on Dashboard/Locks" };
                    return client.replyMessage(rToken, echo);

                 }
                }

               });


                        break;

                        case "lockon":
                        //lock
                        var evtObject= {
                             "status":"0"
                        };
                        var jsondata={

                                 "hub_id":hub_id,
                                 "device_b_one_id":dtslice[0],
                                 "device_id":dtslice[2],
                                 "origin_id":1,
                                 "reqObject":evtObject
                                }
                             logger.info("json lock on 1 data is"+JSON.stringify(jsondata));

               var options = {
                                     url: "https://line.b1automation.com/event/sendEvent",
                                     method: 'POST',
                                     port:443,
                                     headers: {
                                       'Content-Type': 'application/json',
                                       'Accept':'application/json',

                                     },
                                     json:jsondata
                              };

                      request(options, function(err, res, resbody){

               if(!err)
               {
                  if(res.statusCode == 401)
                  {
                       logger.info("access token changed");
                  }
                  else {


              logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                //echo = { type: 'text', text: "スマートロックは閉まっています 施錠するためにはダッシュボードからスマートロックをタップして下さい" };
                //  echo = { type: 'text', text: "Your lock is now unlocked,To turn lock, click on Dashboard/Locks" };
                echo= {
                           "type": "template",
                           "altText": "Live Smart ",
                           "template": {
                            "type": "buttons",
                            "title":"ロックステータス :",
                            "text": "鍵は開いています",
                            "actions": [
                                {
                                  "type": "postback",
                                  "label": "鍵を閉める",
                                  "data":actionid.replace("lockon","lockoff")
                                }
                            ]
                           }
                           };
                           logger.info("datato check on"+JSON.stringify(echo));
                   return client.replyMessage(rToken, echo);

                }
               }

              });


                        break;

                        case "lockoff":
                         // unlock
                         var evtObject= {
                              "status":"1"
                         };
                         var jsondata={

                                  "hub_id":hub_id,
                                  "device_b_one_id":dtslice[0],
                                  "device_id":dtslice[2],
                                  "origin_id":1,
                                  "reqObject":evtObject
                                 }
                              logger.info("json data is"+JSON.stringify(jsondata));

                var options = {
                                      url: "https://line.b1automation.com/event/sendEvent",
                                      method: 'POST',
                                      port:443,
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept':'application/json',

                                      },
                                      json:jsondata
                               };

                       request(options, function(err, res, resbody){

                if(!err)
                {
                   if(res.statusCode == 401)
                   {
                        logger.info("access token changed");
                   }
                   else {


               logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                //  echo = { type: 'text', text: "スマートロックは開きました 施錠するためにはダッシュボードからスマートロックをタップして下さい" };
                //   echo = { type: 'text', text: "Your lock is now locked,To turn unlock, click on Dashboard/Locks" };
                echo= {
                           "type": "template",
                           "altText": "Live Smart ",
                           "template": {
                            "type": "buttons",
                            "title":"ロックステータス :",
                            "text": "鍵は閉まっています ",
                            "actions": [
                                {
                                  "type": "postback",
                                  "label": "鍵を明ける",
                                  "data":actionid.replace("lockoff","lockon")
                                }
                            ]
                           }
                           };
                           logger.info("datato check on"+JSON.stringify(echo));
                    return client.replyMessage(rToken, echo);

                 }
                }

               });


                        break;


                        case "camon":
                         // unlock
                         var evtObject= {
                              "cam_status":"1",
                              "cam_id":dtslice[3]
                         };
                         var jsondata={

                                  "hub_id":hub_id,
                                  "device_b_one_id":dtslice[0],
                                  "device_id":dtslice[2],
                                  "reqObject":evtObject
                                 }
                              logger.info("json data is"+JSON.stringify(jsondata));

                var options = {
                                      url: "https://ls.b1automation.com/event/besStatus",
                                      method: 'POST',
                                      port:443,
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept':'application/json',

                                      },
                                      json:jsondata
                               };

                       request(options, function(err, res, resbody){

                if(!err)
                {
                   if(res.statusCode == 401)
                   {
                        logger.info("access token changed");
                   }
                   else {


                  logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                  // echo = { type: 'text', text: "監視カメラはONになりました. OFFにするためにはダッシュボードから監視カメラをタップして下さい" };
                  // echo = { type: 'text', text: "Your camera is now on,To turn off, click Dashboard/Camera" };
                  echo= {
                             "type": "template",
                             "altText": "Live Smart ",
                             "template": {
                              "type": "buttons",
                              "title":"カメラステータス：",
                              "text": "ON",
                              "actions": [
                                  {
                                    "type": "postback",
                                    "label": "OFFにする",
                                    "data":actionid.replace("camon","camoff")
                                  }
                              ]
                             }
                             };
                             logger.info("datato check on"+JSON.stringify(echo));
                    return client.replyMessage(rToken, echo);

                 }
                }

               });


                        break;

                        case "camoff":
                         // unlock
                         var evtObject= {
                           "cam_status":"0",
                           "cam_id":dtslice[3]
                         };
                         var jsondata={

                                  "hub_id":hub_id,
                                  "device_b_one_id":dtslice[0],
                                  "device_id":dtslice[2],
                                  "reqObject":evtObject
                                 }
                              logger.info("json data is"+JSON.stringify(jsondata));

                var options = {
                                      url: "https://ls.b1automation.com/event/besStatus",
                                      method: 'POST',
                                      port:443,
                                      headers: {
                                        'Content-Type': 'application/json',
                                        'Accept':'application/json',

                                      },
                                      json:jsondata
                               };

                       request(options, function(err, res, resbody){

                if(!err)
                {
                   if(res.statusCode == 401)
                   {
                        logger.info("access token changed");
                   }
                   else {


               logger.info("data upadted sucessfully"+JSON.stringify(resbody));
               // echo = { type: 'text', text: "監視カメラはOFFになりました, ONにするためにはダッシュボードから監視カメラをタップして下さい" };
                  // echo = { type: 'text', text: "Your camera is now off,To turn on, click Dashboard/Camera" };
                  echo= {
                             "type": "template",
                             "altText": "Live Smart ",
                             "template": {
                              "type": "buttons",
                              "title":"カメラステータス :",
                              "text": "OFF",
                              "actions": [
                                  {
                                    "type": "postback",
                                    "label": "ONにする",
                                    "data":actionid.replace("camoff","camon")
                                  }
                              ]
                             }
                             };
                    return client.replyMessage(rToken, echo);

                 }
                }

               });


                        break;

                        case "con":
                        //lock
                        var evtObject= {
                             "status":"1"
                        };
                        var jsondata={

                                 "hub_id":hub_id,
                                 "device_b_one_id":dtslice[0],
                                 "device_id":dtslice[2],
                                 "origin_id":1,
                                 "reqObject":evtObject
                                }
                             logger.info("json lock on 1 data is"+JSON.stringify(jsondata));

               var options = {
                                     url: "https://line.b1automation.com/event/sendEvent",
                                     method: 'POST',
                                     port:443,
                                     headers: {
                                       'Content-Type': 'application/json',
                                       'Accept':'application/json',

                                     },
                                     json:jsondata
                              };

                      request(options, function(err, res, resbody){

               if(!err)
               {
                  if(res.statusCode == 401)
                  {
                       logger.info("access token changed");
                  }
                  else {


              logger.info("data upadted sucessfully"+JSON.stringify(resbody));
               //    echo = { type: 'text', text: "照明はON、OFFする" };
               echo= {
                          "type": "template",
                          "altText": "Live Smart ",
                          "template": {
                           "type": "buttons",
                           "title":"照明ステータス",
                           "text": "照明がONになりました！",
                           "actions": [
                               {
                                 "type": "postback",
                                 "label": "OFFにする",
                                 "data":actionid.replace("con","coff")
                               }
                           ]
                          }
                          };
                          logger.info("datato check on"+JSON.stringify(echo));
                   return client.replyMessage(rToken, echo);

                }
               }

              });


                        break;


                        case "coff":
                        //lock
                        var evtObject= {
                             "status":"0"
                        };
                        var jsondata={

                                 "hub_id":hub_id,
                                 "device_b_one_id":dtslice[0],
                                 "device_id":dtslice[2],
                                 "origin_id":1,
                                 "reqObject":evtObject
                                }
                             logger.info("json lock on 1 data is"+JSON.stringify(jsondata));

               var options = {
                                     url: "https://line.b1automation.com/event/sendEvent",
                                     method: 'POST',
                                     port:443,
                                     headers: {
                                       'Content-Type': 'application/json',
                                       'Accept':'application/json',

                                     },
                                     json:jsondata
                              };

                      request(options, function(err, res, resbody){

               if(!err)
               {
                  if(res.statusCode == 401)
                  {
                       logger.info("access token changed");
                  }
                  else {


              logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                //  echo = { type: 'text', text: "照明はOFF、ONする" };
                echo= {
                           "type": "template",
                           "altText": "Live Smart ",
                           "template": {
                            "type": "buttons",
                            "title":"照明ステータス",
                            "text": "照明がOFFになりました！",
                            "actions": [
                                {
                                  "type": "postback",
                                  "label": "ONにする",
                                  "data":actionid.replace("coff","con"),
                                }
                            ]
                           }
                           };
                             logger.info("datato check"+JSON.stringify(echo));
                   return client.replyMessage(rToken, echo);

                }
               }

              });


                        break;

           case "bulbon":
           case "groupon":
           var query={"hub_id":hub_id,"device_b_one_id":dtslice[0]};
           db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
           logger.error("sujit phlipse at control on"+ JSON.stringify(data))

             var http_url=" ";
             var http_host=data[0].ip_address;

                    if(dtslice[1] === "bulbon")
                    {
                      http_url="/api/"+data[0].username+"/lights/"+data[0].luminaireuniqueid+"/state";
                      actionidopp=actionid.replace("bulbon","bulboff");
                    }
                    else {
                        http_url="/api/"+data[0].username+"/groups/"+data[0].uniqueid+"/action";
                        actionidopp=actionid.replace("groupon","groupoff");
                    }
                    var evtObject= {
                         "http_body":{
                             "on":true
                         },
                         "http_host":http_host,
                         "http_URI":http_url
                    };
                    var jsondata={

                             "hub_id":hub_id,
                             "device_b_one_id":dtslice[0],
                             "device_id":dtslice[2],
                             "origin_id":1,
                             "reqObject":evtObject
                            }
                         logger.info("json data at this point"+JSON.stringify(jsondata));
                         var options = {
                                               url: "https://line.b1automation.com/event/sendEvent",
                                               method: 'POST',
                                               port:443,
                                               headers: {
                                                 'Content-Type': 'application/json',
                                                 'Accept':'application/json',

                                               },
                                               json:jsondata
                                        };
                                        request(options, function(err, res, resbody){

                                 if(!err)
                                 {
                                    if(res.statusCode == 401)
                                    {
                                         logger.info("access token changed");
                                    }
                                    else {


                                logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                                  //  echo = { type: 'text', text: "照明はON、OFFする" };
                                  echo= {
                                             "type": "template",
                                             "altText": "Live Smart ",
                                             "template": {
                                              "type": "buttons",
                                              "title":"照明ステータス",
                                              "text": "照明がONになりました！",
                                              "actions": [
                                                  {
                                                    "type": "postback",
                                                    "label": "OFFにする",
                                                    "data":actionidopp
                                                  }
                                              ]
                                             }
                                             };
                                             logger.info("datato check on"+JSON.stringify(echo));
                                     return client.replyMessage(rToken, echo);

                                  }
                                 }

                                });






             },1);


           break;


           case "bulboff":
           case "groupoff":

           var query={"hub_id":hub_id,"device_b_one_id":dtslice[0]};
           db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
           logger.error("sujit phlipse at control off "+ JSON.stringify(data))

           var http_url=" ";
           var http_host=data[0].ip_address;

                  if(dtslice[1] === "bulboff")
                  {
                    http_url="/api/"+data[0].username+"/lights/"+data[0].luminaireuniqueid+"/state";
                      actionidopp=actionid.replace("bulboff","bulbon");
                  }
                  else {
                      http_url="/api/"+data[0].username+"/groups/"+data[0].uniqueid+"/action";
                      actionidopp=actionid.replace("groupoff","groupon");
                  }

                  var evtObject= {
                       "http_body":{
                           "on":false
                       },
                       "http_host":http_host,
                       "http_URI":http_url
                  };
                  var jsondata={

                           "hub_id":hub_id,
                           "device_b_one_id":dtslice[0],
                           "device_id":dtslice[2],
                           "origin_id":1,
                           "reqObject":evtObject
                          }
                       logger.info("json lock on 1 data is"+JSON.stringify(jsondata));
                       var options = {
                                             url: "https://line.b1automation.com/event/sendEvent",
                                             method: 'POST',
                                             port:443,
                                             headers: {
                                               'Content-Type': 'application/json',
                                               'Accept':'application/json',

                                             },
                                             json:jsondata
                                      };

                                      request(options, function(err, res, resbody){

                               if(!err)
                               {
                                  if(res.statusCode == 401)
                                  {
                                       logger.info("access token changed");
                                  }
                                  else {


                              logger.info("data upadted sucessfully"+JSON.stringify(resbody));
                                //  echo = { type: 'text', text: "照明はOFF、ONする" };
                                echo= {
                                           "type": "template",
                                           "altText": "Live Smart ",
                                           "template": {
                                            "type": "buttons",
                                            "title":"照明ステータス ",
                                            "text": "照明がOFFになりました！",
                                            "actions": [
                                                {
                                                  "type": "postback",
                                                  "label": "ONにする",
                                                  "data":actionidopp
                                                }
                                            ]
                                           }
                                           };
                                             logger.info("datato check"+JSON.stringify(echo));
                                   return client.replyMessage(rToken, echo);

                                }
                               }

                              });






             },1);




           break;

           case "switchon":
           //lock
           var evtObject= {
                "status":"1"
           };
           var jsondata={

                    "hub_id":hub_id,
                    "device_b_one_id":dtslice[0],
                    "device_id":dtslice[2],
                    "origin_id":1,
                    "reqObject":evtObject
                   }
                logger.info("json lock on 1 data is"+JSON.stringify(jsondata));

  var options = {
                        url: "https://line.b1automation.com/event/sendEvent",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:jsondata
                 };

         request(options, function(err, res, resbody){

  if(!err)
  {
     if(res.statusCode == 401)
     {
          logger.info("access token changed");
     }
     else {


 logger.info("data upadted sucessfully"+JSON.stringify(resbody));
  //    echo = { type: 'text', text: "照明はON、OFFする" };
  echo= {
             "type": "template",
             "altText": "Live Smart ",
             "template": {
              "type": "buttons",
              "title":"状態 :",
              "text": "dimmer turned on",
              "actions": [
                  {
                    "type": "postback",
                    "label": "click to turn off",
                    "data":actionid.replace("switchon","switchoff")
                  }
              ]
             }
             };
             logger.info("datato check on"+JSON.stringify(echo));
      return client.replyMessage(rToken, echo);

   }
  }

 });


           break;


           case "switchoff":
           //lock
           var evtObject= {
                "status":"0"
           };
           var jsondata={

                    "hub_id":hub_id,
                    "device_b_one_id":dtslice[0],
                    "device_id":dtslice[2],
                    "origin_id":1,
                    "reqObject":evtObject
                   }
                logger.info("json lock on 1 data is"+JSON.stringify(jsondata));

  var options = {
                        url: "https://line.b1automation.com/event/sendEvent",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:jsondata
                 };

         request(options, function(err, res, resbody){

  if(!err)
  {
     if(res.statusCode == 401)
     {
          logger.info("access token changed");
     }
     else {


 logger.info("data upadted sucessfully"+JSON.stringify(resbody));
   //  echo = { type: 'text', text: "照明はOFF、ONする" };
   echo= {
              "type": "template",
              "altText": "Live Smart ",
              "template": {
               "type": "buttons",
               "title":"状態 :",
               "text": "dimmer is turned off ",
               "actions": [
                   {
                     "type": "postback",
                     "label": "click to turn on",
                     "data":actionid.replace("switchoff","switchon"),
                   }
               ]
              }
              };
                logger.info("datato check"+JSON.stringify(echo));
      return client.replyMessage(rToken, echo);

   }
  }

 });


           break;





              }



}



};







Webhook.prototype.ActionCall = function(user_id,hub_id,rToken){

var query;
  var options = {
                        url: "https://line.b1automation.com/device/getDevices",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:{"hub_id":hub_id,"category_type":"Devices"}
                 };

                 request(options, function(err, res, resbody){

          if(!err)
          {

           //logger.info("data from server :"+JSON.stringify(resbody));

                if(resbody.status=== 0)
                {
                  echo = { type: 'text', text: resbody.message };
                   logger.info("data from server :"+JSON.stringify(echo));
                  return client.replyMessage(rToken, echo);
                }
                else {

                     var deviceData=resbody.devices;
                     var requiredData=[];
                    logger.error("count of devices"+ deviceData.length );
                     _.each(deviceData,function(v){

                          if(v.category_id == "91f98abb-7e90-40fa-ae46-8c54c1bbd563")
                          {
                             requiredData.push(v);
                          }


                     });
                     logger.error("intial device count"+ requiredData.length );
                     if(requiredData.length>0)
                     {

                          var i = 0;
                        //  forming the actiondata array
                          var actiondata=[];
                          var actionArraySet=[];
                          var columnData=[];

                         _.each(requiredData,function(rec) {


                             var b={};
                             b.type="postback";
                             b.label=rec.device_name;//+" - "+rec.room_name;
                             b.data= rec.device_b_one_id+"_"+rec.category_id+"_"+rec.device_name;
                             actiondata.push(b);
                             i++;
                             if(i%3 ==0){
                               var actionArray=[];
                               actionArray.push(actiondata[i-1]);
                               actionArray.push(actiondata[i-2]);
                               actionArray.push(actiondata[i-3]);
                               actionArraySet.push(actionArray);
                             }



                         });

                         var remining=(i%3);
                         var emptyCount=3-remining;

                          if(remining>0)
                          {
                           var actionArray=[];
                           actionArray.push(actiondata[i-1]);
                           remining--;
                           if(remining>0)
                           actionArray.push(actiondata[i-2]);

                           while(emptyCount>0)
                           {

                                  var b={};
                                  b.type="uri";
                                  b.label="  ";//+" - "+rec.room_name;
                                  b.uri= "http://example.com/page/222";
                                  actionArray.push(b);
                                  emptyCount--;

                           }

                           actionArraySet.push(actionArray);
                          }



                            var counter=0;


            //logger.error("array set  count"+ actionArraySet.length );
                         _.each(actionArraySet,function(rec) {
                          counter++;
                            if(counter<=5)
                            {
                             var c={};
                            c.title="コマンド ";
                            c.text="コマンドを選択して下さい";
                            c.actions=rec;
                            columnData.push(c);
                           }


                         });




                         //logger.error("device info"+JSON.stringify(actiondata));
                         echo= {
                                    "type": "template",
                                    "altText": "Live Smart ",
                                        "template": {
                                         "type": "carousel",
                                         "columns":columnData

                                          }
                                 };


            logger.info("echo msg ::"+ JSON.stringify(echo));

            return client.replyMessage(rToken, echo);


                     }
                     else {
                         echo = { type: 'text', text: "コマンドは見つかりませんでした" };
                       return client.replyMessage(rToken, echo);
                     }


                }


          }

         });


};








Webhook.prototype.LocksCall = function(user_id,hub_id,rToken){

var query;
  var options = {
                        url: "https://line.b1automation.com/device/getDevices",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:{"hub_id":hub_id,"category_type":"Security"}
                 };

                 request(options, function(err, res, resbody){

          if(!err)
          {

           //logger.info("data from server :"+JSON.stringify(resbody));

                if(resbody.status=== 0)
                {
                  echo = { type: 'text', text: resbody.message };
                   logger.info("data from server :"+JSON.stringify(echo));
                  return client.replyMessage(rToken, echo);
                }
                else {

                     var deviceData=resbody.devices;
                     var requiredData=[];
                    logger.error("count of devices"+ deviceData.length );
                     _.each(deviceData,function(v){

                          if(v.category_id === "7c076a49-43d7-4e26-a66f-a4b73142d06e" || v.category_id === "24a4d165-f524-4943-ab99-9a5e9a9bf3f4")
                          {
                             requiredData.push(v);
                          }


                     });
                     //logger.error("final device count"+ requiredData.length );
                     if(requiredData.length>0)
                     {
                       var actiondata=[];

                         _.each(requiredData,function(rec) {
                            var b={};
                            b.type="postback";
                            //b.label=rec.device_name;//+"("+rec.room_name+")";
                            b.label=rec.device_name.substring(0, 16);
                            b.data= rec.device_b_one_id+"_"+rec.category_id;
                            actiondata.push(b);
                         });

                         logger.error("device info"+JSON.stringify(actiondata));
                         echo= {
                                    "type": "template",
                                    "altText": "Live Smart ",
                                    "template": {
                                     "type": "buttons",
                                    // "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
                                    //  "title": "Locks",
                                    //  "text":"Please Select Your Lock",
                                    "title": "ロック",
                                    "text":"ロックを選択して下さい",
                                     "actions": actiondata
                                    }
                                 };


            logger.info("echo msg ::"+ JSON.stringify(echo));

            return client.replyMessage(rToken, echo);


                     }
                     else {
                      // echo = { type: 'text', text: "Sorry, i did not find any locks in your LS account" };
                      echo = { type: 'text', text: "ロックは見つかりませんでした" };
                       return client.replyMessage(rToken, echo);
                     }


                }


          }

         });


};



Webhook.prototype.showDeviceStatus = function(did,hub_id,typval,rToken){

  var query={"hub_id":hub_id,"device_b_one_id":did};
  var statusval;
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){

      logger.error("sujit test"+ JSON.stringify(data));
      logger.error("sujit test type "+ typval);
      logger.error("date is"+new Date(data[0].timestamp));
       if(typval=="door")
       {
            if(data[0].status === "1")
            {
              //statusval="Current Status : Door Open"
              statusval="状態 : ドアを開ける"
            }
            else {
              //statusval="Current Status : Door Closed"
              statusval="状態：ドアは閉まっています"
            }

            echo= {
                       "type": "template",
                       "altText": "Live Smart ",
                       "template": {
                        "type": "buttons",
                        "title":statusval,
                        "text": data[0].timestamp,
                        "actions": [
                            {
                              "type": "postback",
                              "label": "   ",
                              "data":  "   "
                            }
                        ]
                       }
                       };


       }
       else {
         var tempval,luxval,humval,uvval;
         tempval=Math.round(parseFloat(data[0].temperature_F));
         humval =Math.round(parseFloat(data[0].humidity));
         luxval=Math.round(parseFloat(data[0].light_intensity));
         uvval=Math.round(parseFloat(data[0].uv_level));
         if(data[0].status === "1")
         {
           //statusval="Motion Detected"
           statusval="状態： モーションを検知"
         }
         else {
           //statusval=" Status: All OK"
           statusval="状態：All OK"
         }

         echo=   {
                    "type": "template",
                    "altText": "Live Smart ",
                    "template": {
                     "type": "buttons",
                     "title": statusval,
                     "text": data[0].timestamp,
                     "actions": [
                         {
                           "type": "postback",
                          // "label": "Temperature :"+tempval+" F",
                          "label": "温度： "+tempval+" F",
                           "data":  "111"
                         },
                         {
                           "type": "postback",
                           //"label": "Humidity : "+humval+" RH",
                            "label": "湿度："+humval+" RH",
                           "data":  "112"
                         },
                         {
                           "type": "postback",
                           //"label": "Light Level :"+luxval+" Lux",
                            "label": "明るさ："+luxval+" Lux",
                           "data":  "113"
                         },
                         {
                           "type": "postback",
                           //"label": "UV Index : "+uvval,
                           "label": "紫外線指数："+uvval,
                           "data":  "114"
                         }

                     ]
                    }
                    };


       }






logger.info("echo msg ::"+ JSON.stringify(echo));

return client.replyMessage(rToken, echo);



    },1);


};



Webhook.prototype.showActionStatus = function(did,hub_id,typval,rToken,actionname){

  var query={"hub_id":hub_id,"device_b_one_id":did};
  var statusval;
  var actionstsid;
  var lbl1;
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
  logger.error("sujit test"+ JSON.stringify(data))
       if(typval=="action")
       {
           var actiontype=data[0].actionInputDevice.cmdType;
           logger.info("cmdType is "+ actiontype);

            if(actiontype == "02")
            {
              statusval="コマンド： "+actionname;
              //lbl1="Click to enable";
              lbl1="コマンド  を有効にする";
              actionstsid=did+"_enable_"+data[0].device_id;
            }

          else  if(data[0].action_status=== "true")
            {
              statusval="コマンド："+actionname;
              //lbl1="Click to disable";
              lbl1="コマンド "+" を無効にする";
              actionstsid=did+"_disable_"+data[0].device_id;

            }
            else {
              statusval="コマンド： "+actionname;
              //lbl1="Click to enable";
              lbl1="コマンド   を有効にする";
              actionstsid=did+"_enable_"+data[0].device_id;

            }
       }
       else //if(typval=="lock"){
         {

         if(data[0].status === "1")
         {
           //statusval="Status : Unlocked";
           statusval="ロックステータス：開いています";
           //lbl1="Click to Lock";
            lbl1="施錠する";
           actionstsid=did+"_lockon_"+data[0].device_id;
           //actionstsid=did;
         }
         else {
           //statusval="Status: Locked";
           statusval="ロックステータス：閉まっています";
           lbl1="解錠する";
          // lbl1="Click to unlock";
           //actionstsid=did;
           actionstsid=did+"_lockoff_"+data[0].device_id;
         }

       }



    echo= {
               "type": "template",
               "altText": "Live Smart ",
               "template": {
                "type": "buttons",
                "title": statusval,
                "text": data[0].timestamp,
                "actions": [
                    {
                      "type": "postback",
                      "label": lbl1,
                      "data":  actionstsid
                    }

                ]
               }
               };


logger.info("echo msg ::"+ JSON.stringify(echo));

return client.replyMessage(rToken, echo);



    },1);


};


Webhook.prototype.showSwitchStatus = function(did,hub_id,typval,rToken,actionname){

  var query={"hub_id":hub_id,"device_b_one_id":did};
  var statusval;
  var actionstsid;
  var lbl1;
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
  logger.error("sujit test"+ JSON.stringify(data))
       if(typval=="switch")
       {
            if(data[0].status=== "1")
            {
              statusval="switch turned on";
              //lbl1="Click to disable";
              lbl1="click to turn off";
              actionstsid=did+"_switchoff_"+data[0].device_id;

            }
            else {
              statusval="switch turned off";
              //lbl1="Click to enable";
              lbl1="click to turn on";
              actionstsid=did+"_switchon_"+data[0].device_id;

            }
       }
       else //if(typval=="lock"){
         {

         if(data[0].status === "1")
         {
           //statusval="Status : Unlocked";
           statusval="状態：dimmer turned on";
           //lbl1="Click to Lock";
            lbl1="click to turn off ";
           actionstsid=did+"_switchoff_"+data[0].device_id;
           //actionstsid=did;
         }
         else {
           //statusval="Status: Locked";
           statusval="状態：dimmer turned off";
           lbl1="click to turn on";
          // lbl1="Click to unlock";
           //actionstsid=did;
           actionstsid=did+"_switchon_"+data[0].device_id;
         }

       }



    echo= {
               "type": "template",
               "altText": "Live Smart ",
               "template": {
                "type": "buttons",
                "title": statusval,
                "text": data[0].timestamp,
                "actions": [
                    {
                      "type": "postback",
                      "label": lbl1,
                      "data":  actionstsid
                    }

                ]
               }
               };


logger.info("echo msg ::"+ JSON.stringify(echo));

return client.replyMessage(rToken, echo);



    },1);


};



Webhook.prototype.CameraCall = function(user_id,hub_id,rToken){

var query;
  var options = {
                        url: "https://line.b1automation.com/device/getDevices",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:{"hub_id":hub_id,"category_type":"Security"}
                 };

                 request(options, function(err, res, resbody){

          if(!err)
          {

           //logger.info("data from server :"+JSON.stringify(resbody));

                if(resbody.status=== 0)
                {
                  echo = { type: 'text', text: resbody.message };
                   logger.info("data from server :"+JSON.stringify(echo));
                  return client.replyMessage(rToken, echo);
                }
                else {

                     var deviceData=resbody.devices;
                     var requiredData=[];
                    logger.error("count of devices"+ deviceData.length );
                     _.each(deviceData,function(v){

                          if(v.category_id === "251b4980-02ca-44f4-8331-219896bf72cf")
                          {
                             requiredData.push(v);
                          }


                     });
                     //logger.error("final device count"+ requiredData.length );
                     if(requiredData.length>0)
                     {
                       var actiondata=[];

                         _.each(requiredData,function(rec) {
                            var b={};
                            b.type="postback";
                            //b.label=rec.device_name;//+"("+rec.room_name+")";
                            b.label=rec.device_name.substring(0, 16);
                            b.data= rec.device_b_one_id+"_"+rec.category_id;
                            actiondata.push(b);
                         });

                         logger.error("device info"+JSON.stringify(actiondata));
                         echo= {
                                    "type": "template",
                                    "altText": "Live Smart ",
                                    "template": {
                                     "type": "buttons",
                                    // "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
                                     //"title": "Camera",
                                     //"text":"Please Select Your Camera",
                                     "title": "カメラ",
                                     "text":"カメラを選択して下さい",
                                     "actions": actiondata
                                    }
                                 };


            logger.info("echo msg ::"+ JSON.stringify(echo));

            return client.replyMessage(rToken, echo);


                     }
                     else {
                       //echo = { type: 'text', text: "Sorry, i did not find any Cameras  in your LS account" };
                       echo = { type: 'text', text: "カメラが見つかりません" };
                       return client.replyMessage(rToken, echo);
                     }


                }


          }

         });


};


Webhook.prototype.showCameraStatus = function(did,hub_id,typval,rToken,actionname){

  var query={"hub_id":hub_id,"device_b_one_id":did};
  var statusval;
  var actionstsid;
  var lbl1;
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
  logger.error("sujit test"+ JSON.stringify(data))
       if(typval=="cam")
       {
            if(data[0].status === "1")
            {
              statusval="カメラステータス： ON";
              lbl1="OFFにする";
            //  statusval="Status : Camera On ";
            //  lbl1="Click to Off";
              actionstsid=did+"_camoff_"+data[0].device_id+"_"+data[0].vcam_id;

            }
            else if(data[0].status === "0") {
            //  statusval="Status: Camera Off";
              //lbl1="Click to On";
              statusval="カメラステータス：OFF";
              lbl1="ONにする";
              actionstsid=did+"_camon_"+data[0].device_id+"_"+data[0].vcam_id;

            }
       }
       else
         {

         if(data[0].status === "1")
         {
           statusval="照明ステータス：ON";
           lbl1="OFFする ";
           actionstsid=did+"_coff_"+data[0].device_id;
           //actionstsid=did;
         }
         else {
           statusval="照明ステータス：OFF";
           lbl1="ONする";
           //actionstsid=did;
           actionstsid=did+"_con_"+data[0].device_id;
         }

       }



    echo= {
               "type": "template",
               "altText": "Live Smart ",
               "template": {
                "type": "buttons",
                "title": statusval,
                "text": data[0].timestamp,
                "actions": [
                    {
                      "type": "postback",
                      "label": lbl1,
                      "data":  actionstsid
                    }

                ]
               }
               };


logger.info("echo msg ::"+ JSON.stringify(echo));

return client.replyMessage(rToken, echo);



    },1);


};



Webhook.prototype.showPhilpseStatus = function(did,hub_id,typval,rToken,actionname){

  var query={"hub_id":hub_id,"device_b_one_id":did};
  var statusval;
  var actionstsid;
  var lbl1;
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
  logger.error("sujit test"+ JSON.stringify(data))
  logger.error("sujit test buld"+ typval);
       if(typval === "bulb")
       {

            if(data[0].on === "1" || data[0].on === true)
            {

              statusval="照明ステータス：ON ";
              lbl1="OFFする";
              actionstsid=did+"_bulboff_"+data[0].device_id;

            }
            else  {
              statusval="照明ステータス：OFF";
              lbl1="ONする";
              actionstsid=did+"_bulbon_"+data[0].device_id;

            }
       }
       else
         {

         if(data[0].on === "1" || data[0].on === true)
         {
           statusval="照明ステータス：ON";
           lbl1="OFFする";
           actionstsid=did+"_groupoff_"+data[0].device_id;
           //actionstsid=did;
         }
         else {
           statusval="照明ステータス：OFF";
           lbl1="ONする";
           //actionstsid=did;
           actionstsid=did+"_groupon_"+data[0].device_id;
         }

       }



    echo= {
               "type": "template",
               "altText": "Live Smart ",
               "template": {
                "type": "buttons",
                "title": statusval,
                "text": data[0].timestamp,
                "actions": [
                    {
                      "type": "postback",
                      "label": lbl1,
                      "data":  actionstsid
                    }

                ]
               }
               };


logger.info("echo msg ::"+ JSON.stringify(echo));

return client.replyMessage(rToken, echo);



    },1);


};



     Webhook.prototype.MenuCall = function(rToken,lineId){
    var cpin;
     var that=this;
      that.getModels();

      that.lineModel.findAll({where: {line_id: lineId}})
      .then(function(user){

         if(user.length>0)
         {
           cpin=user[0].state;
           echo= {
           "type": "template",
           "altText": "Live Smart ",
           "template": {
               "type": "buttons",
               "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
               "title": "メニュー",
               "text": "メニューを選んで下さい",
               "actions": [
                           {
                             "type": "uri",
                             "label": "アカウントマネジメント",
                             "uri": "http://blazeinnovations.com/WebApp/ls-line/#/manageaccount?"+cpin
                           },
                             {
                               "type": "uri",
                               "label": "ゲスト追加",
                               "uri": "http://blazeinnovations.com/WebApp/ls-line/#/addguest?"+cpin
                             },
                             {
                               "type": "uri",
                               "label": "スタンプのダウンロード",
                               "uri": "https://www.livesmart.jp"
                             },
                             {
                               "type": "uri",
                               "label": "Help",
                               "uri": "https://www.youtube.com/watch?v=m83mkOdEEIk&feature=youtu.be"
                             }

                        ]
                   }
            }
            logger.info("echo msg ::"+ JSON.stringify(echo));
         return client.replyMessage(rToken, echo);
         }


      });

     };





     Webhook.prototype.ByeCall = function(user_id,hub_id,lineId,rToken){


       var that=this;
       that.getModels();

       that.lineModel.findAll({where: {line_id: lineId}})
                       .then(function(user){
                            logger.error("user id  to logout "+ user[0].user_id);
                           if(user.length>0)
                           {

                             that.lineModel.destroy({where: {hub_id: hub_id}})
                                 .then(function(){
                                  //   echo = { type: 'text', text: "Thanks for useing LS Bot" };
                                  echo = { type: 'text', text: "ご利用有難うございました" };
                                     return client.replyMessage(rToken, echo);
                                 });

                           }
                           else {
                             //echo = { type: 'text', text: "Good Bye" };
                              echo = { type: 'text', text: "さよなら" };
                             return client.replyMessage(rToken, echo);
                           }


                       });

     };




     Webhook.prototype.postLogin = function (req, res) {
         logger.trace('postLogin start');
         var body = req.body;
         logger.info("data received from the web is "+JSON.stringify(body));
         var obj = {email_id: body.email.toLowerCase()};
         that = this;
         that.getModels();
         that.userModel.findAll({where: obj})
             .then(function (arr) {
                   if (arr.length === 0) {
                     body.title = "Live Smart";
                     body.message = "Emailとパスワードは有効ではありません、も一度試して下さい";
                     res.render('line.html', body);

                   }
                   else {

                     user = arr[0];
                     user_id=arr[0].user_id;
                     var customer_pin=arr[0].customer_pin;
                     if(user){
                       if (user.password === that.encryptPassword(body.password)|| user.otp === that.encryptPassword(body.password)){
                           logger.trace('Login success');
                           that.userHubModel.findAll({where: {user_id:user_id} })
                             .then(function (userhub) {
                               if(userhub[0])
                               {
                              hub_id=userhub[0].hub_id;

                              that.lineModel.build({user_id: user_id, hub_id: hub_id,line_id :body.state ,email_id:body.email.toLowerCase(),state:customer_pin}).save()
                                  .then(function(){

                                    //that.securityCall(user_id,hub_id,event.replyToken);
                                  //  echo={ type: 'text', text: "Thank you for verifying your email address. Start using the dashboard, or ask me about your LS."};
                                  //  logger.error("data sending :" + JSON.stringify(echo));
                                  //  body.title = "Live Smart";
                                  //  body.message = "Thank you for verifying your email address. Start using the dashboard, or ask me about your LS.";
                                    // body.message = "メールアドレスの認証ありがとうございます,まずダッシュボードから始めてみましょう、またはLSについて私に聞いて下さい";
                                    //res.render('line.html', body);

                        echo={ type: 'text', text: "Email IDが確認できました。最初にダッシュボードを開いてみましょう。LS について質問があればどうぞ。  https://www.youtube.com/watch?v=m83mkOdEEIk&feature=youtu.be"};
                                    logger.error("data sending :" + JSON.stringify(echo));
                                    client.pushMessage(body.state, echo);
                                    body.title = "Live Smart";
                                  //  body.message="sucess";
                                    body.msgstatus = "メールアドレスの認証ありがとうございます,まずダッシュボードから始めてみましょう、またはLSについて私に聞いて下さい";
                                    res.render('loginsuccess.html', body);

                                  });
                              }
                              else {
                                body.title = "Live Smart";
                                body.message = "Emailまたはパスワードが間違っています。もう一度入力して下さい。";
                                res.render('line.html', body);

                              }
                             });


                       }else{
                         //TODO: Password not match
                         body.title = "Live Smart";
                         body.message = "パスワードは有効ではありません、も一度試して下さい";
                         res.render('line.html', body);
                       }
                     }


                   }




             })
             .catch(function(err){
                 logger.error(err.message);
                 body.title = "Live Smart";
                 body.message = "有効できませんでした、も一度試して下さい";
                 res.render('line.html', body);
             });


         logger.trace('postLogin End');
     };

     Webhook.prototype.encryptPassword = function(id) {
         var sha = crypto.createHash('md5');
         sha.update(id);
         return sha.digest('hex');
     };

     Webhook.prototype.getLogin = function (req, res) {
         var responseData = req.query;
         responseData.title = "Live Smart";
         //logger.trace('resdatat  start'+ JSON.stringify(responseData));
         res.render('line.html', responseData);

     };


     Webhook.prototype.besimage = function (req, res) {
         var responseData = req.query;

         var query={"hub_id":req.query.hub_id,"device_b_one_id":req.query.boneid};
         db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
            logger.error("sujit test"+ JSON.stringify(data));
            responseData.title = "Live Smart";
            responseData.urldata=data[0].latest_thumbnail_url;
            logger.trace('resdatat  start'+ JSON.stringify(responseData));
            res.render('beseye.html', responseData);
         },1);

     };



     //lights call
     Webhook.prototype.Lightscall = function(user_id,hub_id,rToken){

       var query;
       var options = {
                             url: "https://line.b1automation.com/device/getDevices",
                             method: 'POST',
                             port:443,
                             headers: {
                               'Content-Type': 'application/json',
                               'Accept':'application/json',

                             },
                             json:{"hub_id":hub_id,"category_type":"Devices"}
                      };

                      request(options, function(err, res, resbody){

               if(!err)
               {

                //logger.info("data from server :"+JSON.stringify(resbody));

                     if(resbody.status=== 0)
                     {
                       echo = { type: 'text', text: resbody.message };
                        logger.info("data from server :"+JSON.stringify(echo));
                       return client.replyMessage(rToken, echo);
                     }
                     else {

                          var deviceData=resbody.devices;
                          var requiredData=[];
                         logger.error("count of devices"+ deviceData.length );
                          _.each(deviceData,function(v){

                               if(v.category_id === "0c66ae21-f630-4373-ae95-223cd955a6a0" || v.category_id === "78eec792-fe66-41c7-be0f-ff1e958d4b46" || v.category_id === "36bcaaa5-342e-4209-ad49-542bec9e3f38" || v.category_id === "ca9c0678-708e-4961-8a95-98b81d4fa580" )
                               {
                                  requiredData.push(v);
                               }


                          });
                          //logger.error("final device count"+ requiredData.length );
                          if(requiredData.length>0)
                          {
                            var actiondata=[];
                              var i=0;
                              _.each(requiredData,function(rec) {
                                if(i<4)
                                {
                                 var b={};
                                 b.type="postback";
                                 b.label=rec.device_name.substring(0, 16);//+"("+rec.room_name+")";
                                 b.data= rec.device_b_one_id+"_"+rec.category_id;
                                 actiondata.push(b);
                                 i++;
                                }
                              });

                              logger.error("device info"+JSON.stringify(actiondata));
                              echo= {
                                         "type": "template",
                                         "altText": "Live Smart ",
                                         "template": {
                                          "type": "buttons",
                                         // "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
                                          "title": "照明",
                                          "text":"照明を選んで下さい ",
                                          "actions": actiondata
                                         }
                                      };


                 logger.info("echo msg ::"+ JSON.stringify(echo));

                 return client.replyMessage(rToken, echo);


                          }
                          else {
                            //echo = { type: 'text', text: "Sorry, i did not find any Lights in your LS account" };
                            echo = { type: 'text', text: "LS アカウントには照明ありません" };
                            return client.replyMessage(rToken, echo);
                          }


                     }


               }

              });


     };



     Webhook.prototype.showNinjaStatus = function(did,hub_id,typval,rToken,actionname){

       var query={"hub_id":hub_id,"device_b_one_id":did};
       var statusval;
       var actionstsid;
       var lbl1;
       db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
       logger.error("sujit test"+ JSON.stringify(data))
            if(typval=="ninja")
            {
              if(data[0].status === "1")
              {
                //statusval="Status : Unlocked";
                //lbl1="Click to Lock";
                statusval="状態：開いています";
                lbl1="施錠する";
                actionstsid=did+"_ninjaon_"+data[0].device_id;
                //actionstsid=did;
              }
              else {
                //statusval="Status: Locked";
                //lbl1="Click to unlock";
                //actionstsid=did;
                statusval="状態：閉まっています";
                lbl1="解錠する";
                actionstsid=did+"_ninjaoff_"+data[0].device_id;
               }
            }




         echo= {
                    "type": "template",
                    "altText": "Live Smart ",
                    "template": {
                     "type": "buttons",
                     "title": statusval,
                     "text": data[0].timestamp,
                     "actions": [
                         {
                           "type": "postback",
                           "label": lbl1,
                           "data":  actionstsid
                         }

                     ]
                    }
                    };


     logger.info("echo msg ::"+ JSON.stringify(echo));

     return client.replyMessage(rToken, echo);



         },1);


     };








Webhook.prototype.getModels = function(){
    if(this.userModel){
        return ;
    }
    var Models = mysqlDb.getModels();
    this.userModel = Models.User;
    this.userHubModel = Models.UserHub;
    this.userDevModel = Models.UserDev;
    this.hubModel = Models.Hub;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.remoteModel = Models.RemoteKeyInfo;
    this.alexaUserModel =Models.AlexaUser;
    this.lineModel=Models.LineUser;
};

module.exports = new Webhook();
